class AddIbgeData < ActiveRecord::Migration[5.0]
  create_table :ibge do |t|
  	t.string :cod_sencitario, unique: true
  	t.string :nome_municipio
  	t.string :nome_bairro
  	t.float :renda_municipio
  	t.integer :qtd_total_homens
		t.integer :qtd_homens_reponsaveis_domicilio
		t.integer :qtd_homens_00_10
		t.integer :qtd_homens_11_20
		t.integer :qtd_homens_21_30
		t.integer :qtd_homens_31_40
		t.integer :qtd_homens_41_50
		t.integer :qtd_homens_51_60
		t.integer :qtd_homens_61
		t.integer :renda_homens_A
		t.integer :renda_homens_B
		t.integer :renda_homens_C
		t.integer :renda_homens_D
		t.integer :renda_homens_E
		t.integer :qtd_total_mulheres
		t.integer :qtd_mulheres_reponsaveis_domicilio
		t.integer :qtd_mulheres_00_10
		t.integer :qtd_mulheres_11_20
		t.integer :qtd_mulheres_21_30
		t.integer :qtd_mulheres_31_40
		t.integer :qtd_mulheres_41_50
		t.integer :qtd_mulheres_51_60
		t.integer :qtd_mulheres_61
		t.integer :renda_mulheres_A
		t.integer :renda_mulheres_B
		t.integer :renda_mulheres_C
		t.integer :renda_mulheres_D	
		t.integer :renda_mulheres_E
		t.integer :qtd_domicilios
		t.integer :qtd_familias
		t.float :renda_domicilios
	end
end
