class Usuario < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :chat_rooms, dependent: :destroy
  has_many :messages, dependent: :destroy
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :omniauthable #:confirmable,
  has_many :anuncio, dependent: :destroy
  #before_action :verify_authenticity_token, :only => [:index]

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |usuario|
      usuario.email = auth.info.email
      usuario.name = auth.info.name
      usuario.password = Devise.friendly_token[0,20]
      usuario.image = auth.info.image
    end
  end

end
