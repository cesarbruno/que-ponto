require 'test_helper'

class PontoComercialsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ponto_comercial = ponto_comercials(:one)
  end

  test "should get index" do
    get ponto_comercials_url
    assert_response :success
  end

  test "should get new" do
    get new_ponto_comercial_url
    assert_response :success
  end

  test "should create ponto_comercial" do
    assert_difference('PontoComercial.count') do
      post ponto_comercials_url, params: { ponto_comercial: { banheiros: @ponto_comercial.banheiros, descricao: @ponto_comercial.descricao, metros_quadrados: @ponto_comercial.metros_quadrados, qtd_ambientes: @ponto_comercial.qtd_ambientes, seg_mercado: @ponto_comercial.seg_mercado, tipo_imovel: @ponto_comercial.tipo_imovel, tipo_negocio: @ponto_comercial.tipo_negocio, titulo: @ponto_comercial.titulo, vagas: @ponto_comercial.vagas, vagas_visitantes: @ponto_comercial.vagas_visitantes } }
    end

    assert_redirected_to ponto_comercial_url(PontoComercial.last)
  end

  test "should show ponto_comercial" do
    get ponto_comercial_url(@ponto_comercial)
    assert_response :success
  end

  test "should get edit" do
    get edit_ponto_comercial_url(@ponto_comercial)
    assert_response :success
  end

  test "should update ponto_comercial" do
    patch ponto_comercial_url(@ponto_comercial), params: { ponto_comercial: { banheiros: @ponto_comercial.banheiros, descricao: @ponto_comercial.descricao, metros_quadrados: @ponto_comercial.metros_quadrados, qtd_ambientes: @ponto_comercial.qtd_ambientes, seg_mercado: @ponto_comercial.seg_mercado, tipo_imovel: @ponto_comercial.tipo_imovel, tipo_negocio: @ponto_comercial.tipo_negocio, titulo: @ponto_comercial.titulo, vagas: @ponto_comercial.vagas, vagas_visitantes: @ponto_comercial.vagas_visitantes } }
    assert_redirected_to ponto_comercial_url(@ponto_comercial)
  end

  test "should destroy ponto_comercial" do
    assert_difference('PontoComercial.count', -1) do
      delete ponto_comercial_url(@ponto_comercial)
    end

    assert_redirected_to ponto_comercials_url
  end
end
