class AddSecondUserToConversation < ActiveRecord::Migration[5.0]
  def change
  	add_reference :chat_rooms, :usuario2, index: true
  end
end
