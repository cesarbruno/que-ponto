class Anuncio < ApplicationRecord

  has_many :like#, dependent: :destroy
  has_many :preco, dependent: :destroy
  belongs_to :usuario
  
  mount_uploaders :fotos, FotoUploader
  serialize :fotos, JSON
  mount_uploaders :carga_descarga_fotos, FotoCargaDescargaUploaderUploader
  serialize :carga_descarga_fotos, JSON
  mount_uploader :video, VideoUploader
  serialize :video, JSON
  
  

    
  def verify_value(value, max = nil, min = nil)
    if max.nil? && min.nil?
      return true
    elsif max.nil? && !min.nil?
      if value >= min
        return true
      end
    elsif !max.nil? && min.nil?
      if value <= max
        return true
      end
    else
      # !max.nil? && !min.nil?
      if value <= max && value >= min
        return true                                                                                                          
      end                                                                                        
    end
    return false
  end

	# Método de pesquisa
	def self.search(search)
    @results = Array.new
    called_attributes = Array.new
    for parameter in search
      # parameter=value
      par = parameter
      par_value = search[parameter]
      if Anuncio.has_attribute?(par)
        Anuncio.find_each do |anuncio|
          # if match param vs attribute
          if  !anuncio.instance_eval(par).nil? && anuncio.instance_eval(par) == par_value
            called_attributes << parameter
            preco = Preco.where(anuncio_id: anuncio.id).last
            next if preco.nil?
            @results << anuncio if verify_value(preco.preco_ponto, search[:preco_max], search[:preco_min]) or verify_value(preco.preco_aluguel, search[:preco_max], search[:preco_min]) or verify_value(preco.preco_venda, search[:preco_max], search[:preco_min])
          end          
        end
      elsif par == "search" && !par_value.nil? && par_value != ""
        # In this case search for code
        if par_value.strip.to_i > 0
          result = Anuncio.where('codigo LIKE ?',  '%#{par_value.strip.to_i}%')
          regexp = /#{par_value.strip.to_i}/i; # Case-sensitive conversão
          result.sort{|x, y| (x =~ regexp) <=> (y =~ regexp) } # Ordena por melhor resultado
          @results.concat result #concatena as duas arrays
          return @results.uniq
        # In this case search for a address
        else
          search_length = par_value.split.length
          words = Array.new

          for word in par_value.split
            words.push(word)
            words.push(word)
            words.push(word)
          end
          @results.concat Anuncio.where([(['cidade LIKE ? OR bairro LIKE ? OR logradouro LIKE ?'] * search_length).join(' OR ')] + words.map { |name| "%#{name}%" })        
        end
      end
    end

    @results = @results.uniq
    @results.delete_if do |anuncio|
      r = false
      for att in called_attributes
        if anuncio.instance_eval(att) != search[att]
          r = true
          break
        end
      end
      r
    end

    return @results
	end
end