class AddUsuarioToLike < ActiveRecord::Migration[5.0]
  def change
  	change_table :likes do |t|
  		t.integer :usuario
  	end
  end
end
