class AddPicturesToAnuncios < ActiveRecord::Migration[5.0]
  def change
    add_column :anuncios, :fotos, :string
  end
end
