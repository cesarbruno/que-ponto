class Message < ApplicationRecord
  belongs_to :usuario
  belongs_to :ChatRoom

  after_create_commit { MessageBroadcastJob.perform_later(self) }
end
