/*
	Utils => 
		Esse documento é o utilitário de organização dos retorno do Google Maps.
*/


var markers = [];

/*function PRIVATE_clearMarker(){
	// Limpa as marcas antigas.
	markers.forEach(function(marker) {
		marker.setMap(null);
	});
	markers = [];
}*/
function setKmlLayer(map, url){
	var ctaLayer = new google.maps.KmlLayer({
    	url: url,
    	map: map
  	});
}

function PRIVATE_searchBox(map, searchBox){
	/*
		Esta função faz com que o search box do mapa realmente funcione

		map => object (google.maps.Map)
		searchBox = object (google.maps.places.SearchBox)
	*/

	// Avisa e libera o resulta da searchBox no mapa
    map.addListener('bounds_changed', function() {
    	searchBox.setBounds(map.getBounds());
    });
	// Quando o local é alterado no searchBox executa essa função
    searchBox.addListener('places_changed', function() {
	    var places = searchBox.getPlaces();

	    if (places.length == 0) {
	      	return;
	    }
	     // Limpa as marcas antigas.
		PRIVATE_clearMarker()

	    // Pega o nome e a localização de cada lugar.
	    var bounds = new google.maps.LatLngBounds();

	    places.forEach(function(place) {
	      var icon = {
	        url: place.icon,
	        size: new google.maps.Size(71, 71),
	        origin: new google.maps.Point(0, 0),
	        anchor: new google.maps.Point(17, 34),
	        scaledSize: new google.maps.Size(25, 25)
	      };

	      // Cria uma marca para cada lugar.
	      markers.push(new google.maps.Marker({
	        map: map,
	        icon: icon,
	        title: place.name,
	        position: place.geometry.location
	      }));	

	      setStreetView(map, place.geometry.location);


	      if (place.geometry.viewport) {
	        // Only geocodes have viewport.
	        bounds.union(place.geometry.viewport);
	      } else {
	        bounds.extend(place.geometry.location);
	      }
	    });
	    map.fitBounds(bounds);
    });
}

function initMap(){
	/*
		Esta função deve ser chamada toda vez que houver um mapa na página.
		Na ID = "map_canvas";
	*/
	var mapOP = { zoom: 16 };
	map = new google.maps.Map(document.getElementById('map_canvas'), mapOP);

	var pos = new google.maps.LatLng(-8.0538900, -34.8811100);
    map.setCenter(pos);

	//Cria o botão que pega a atual localização
	var geolocationDiv = document.createElement('div');
	geolocationControl(geolocationDiv, map);
	map.controls[google.maps.ControlPosition.TOP_CENTER].push(geolocationDiv);

    // Cria a Search Box e linka ao mapa.
    var input = document.getElementById('pac-input');
    if (input) {
    	var searchBox = new google.maps.places.SearchBox(input);
    	// Seta a utilização da caixa de pesquisa
    	PRIVATE_searchBox(map, searchBox);
    }
    

    
    
	return map;
}

function setStreetView(map, pos){
	var panorama = new google.maps.StreetViewPanorama(
    document.getElementById('street_view_canvas'), {
    	position: pos,
        pov: {
        	heading: 34,
        	pitch: 10
        }
    });
  map.setStreetView(panorama);
}

function getLatLngByClick(map){

	var SetCenterToGetCenter = function (event){
		map.setCenter(event.latLng);
		google.maps.event.removeListener(map, "click",SetCenterToGetCenter);		
		return map.getCenter();
	}
	google.maps.event.addListener(map, "click", SetCenterToGetCenter);


	/*google.maps.event.addListener(map, 'click', function(event) {
		map.setCenter(event.latLng)
  	});
  	return map.getCenter();

  	var clickFunction = function (event) {
	    //do some stuff here
	    window.removeEventListener('click',clickFunction, false );

	};
	window.addEventListener("click", clickFunction, false);*/
}

function removeListener(listener){
	google.maps.event.removeListener(listener);
}


function translateTypes(types){
	/*
		Essa função traduz todos os tipos da entrada

		types => array(string)
	*/
	var tipos = "";
	for (var i = 0; i < types.length; i++) {
		switch(types[i]){
			/*case "accounting":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;*/
			case "airport":
				if (i == 0) {
					tipos = tipos + " Aeroporto e/ou Heliponto";
				}else{
					tipos = tipos + ", aeroporto e/ou heliponto";
				}
				break;
			/*case "amusement_park":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "aquarium":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "art_gallery":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;*/
			case "atm":
				if (i == 0) {
					tipos = tipos + "Caixa Eletrônico";
				}else{
					tipos = tipos + ", caixa eletrônico";
				}
				break;
			/*case "bakery":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;*/
			case "bank":
				if (i == 0) {
					tipos = tipos + "Banco";
				}else{
					tipos = tipos + ", banco";
				}
				break;
			/*case "bar":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "beauty_salon":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "bicycle_store":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "book_store":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "bowling_alley":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;*/
			case "bus_station":
				if (i == 0) {
					tipos = tipos + "Parada de Ônibos";
				}else{
					tipos = tipos + ", parada de ônibus";
				}
				break;
			/*case "cafe":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "campground":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "car_dealer":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "car_rental":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "car_repair":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "car_wash":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "casino":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "cemetery":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;*/
			case "church":
				if (i == 0) {
					tipos = tipos + "Igreja";
				}else{
					tipos = tipos + ", igreja";
				}
				break;
			/*case "city_hall":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "clothing_store":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "convenience_store":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "courthouse":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "dentist":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "department_store":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "doctor":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "electrician":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "electronics_store":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "embassy":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;*/
			case "establishment":
				if (i == 0) {
					tipos = tipos + "Estabelecimento";
				}else{
					tipos = tipos + ", estabelecimento";
				}
				break;
			/*case "finance":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "fire_station":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "florist":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "food":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "funeral_home":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "furniture_store":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "gas_station":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "general_contractor":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "grocery_or_supermarket":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "gym":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "hair_care":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "hardware_store":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "health":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "hindu_temple":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "home_goods_store":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;*/
			case "hospital":
				if (i == 0) {
					tipos = tipos + "Hospital";
				}else{
					tipos = tipos + ", hospital";
				}
				break;
			/*case "insurance_agency":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "jewelry_store":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "laundry":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "lawyer":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "library":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "liquor_store":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "local_government_office":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "locksmith":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "lodging":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "meal_delivery":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "meal_takeaway":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "mosque":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "movie_rental":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "movie_theater":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "moving_company":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "museum":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "night_club":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "painter":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;*/
			case "park":
				if (i == 0) {
					tipos = tipos + "Parque";
				}else{
					tipos = tipos + ", parque";
				}
				break;
			/*case "parking":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "pet_store":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "pharmacy":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "physiotherapist":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "place_of_worship":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "plumber":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "police":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "post_office":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "real_estate_agency":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "restaurant":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "roofing_contractor":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "rv_park":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;*/
			case "school":
				if (i == 0) {
					tipos = tipos + "Escola";
				}else{
					tipos = tipos + ", escola";
				}
				break;
			/*case "shoe_store":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;*/
			case "shopping_mall":
				if (i == 0) {
					tipos = tipos + "Shopping Center";
				}else{
					tipos = tipos + ", shopping center";
				}
				break;
			/*case "spa":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "stadium":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "storage":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "store":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;*/
			case "subway_station":
				if (i == 0) {
					tipos = tipos + "Estação de Metrô";
				}else{
					tipos = tipos + ", estação de metrô";
				}
				break;
			/*case "synagogue":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;*/
			case "taxi_stand":
				if (i == 0) {
					tipos = tipos + "Ponto de Táxi";
				}else{
					tipos = tipos + ", ponto de táxi";
				}
				break;
			/*case "train_station":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;*/
			/*case "travel_agency":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;*/
			case "university":
				if (i == 0) {
					tipos = tipos + "Universidade";
				}else{
					tipos = tipos + ", universidade";
				}
				break;
			/*case "veterinary_care":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;
			case "zoo":
				if (i == 0) {
					tipos = tipos + " ";
				}else{
					tipos = tipos + ", ";
				}
				break;*/
			default:
				break;
		}		
	}
	return tipos;
}

function geolocationControl(controlDiv, map) {
	/*
		Essa função cria o botão de de localização atual
	*/

    // Set CSS for the control button
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#444';
    controlUI.style.borderStyle = 'solid';
    controlUI.style.borderWidth = '1px';
    controlUI.style.borderColor = 'white';
    controlUI.style.height = '28px';
    controlUI.style.marginTop = '5px';
    controlUI.style.cursor = 'pointer';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Meu Local Atual';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control text
    var controlText = document.createElement('div');
    controlText.style.fontFamily = 'Arial,sans-serif';
    controlText.style.fontSize = '10px';
    controlText.style.color = 'white';
    controlText.style.paddingLeft = '10px';
    controlText.style.paddingRight = '10px';
    controlText.style.marginTop = '8px';
    controlText.innerHTML = 'Meu Local Atual';
    controlUI.appendChild(controlText);

    // Setup the click event listeners to geolocate user
    google.maps.event.addDomListener(controlUI, 'click', geolocate);
}

function geolocate() {
	/*
		Essa função pega a atual localização e seta ela no centro do mapa
	*/

    if (navigator.geolocation) {

        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            map.setCenter(pos);
        });
    }else{
    	var pos = new google.maps.LatLng(-8.0538900, -34.8811100);
    	map.setCenter(pos);
    }
}

function newMarker(map, place, array, id){
	var marker = new google.maps.Marker({
		id: id,
		map: map,
		position: place.geometry.location
	});
	//Insere o marker na array markers
	//array.push(marker);
	array[id] = marker;
	marker.setMap(map);
}
function showMarkers(map, markers){
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
	}

}
function removeMarker(markers, place_id){
	markers[place_id].setMap(null);
	markers.splice(place_id, 1);
}

function setRoute(map, originAddress, destinationAddress, waypoints = null){
	/*
		Esta função organiza e imprime a rota na tela

		map 				=> object (google.maps.Map)
		originAddress 		=> array (string || object (ltd lng))
		destinationAddress 	=> string || object (ltd lng)
		waypoints 			=> string || object (ltd lng) || null
			Em caso de origem == null então o endereço de partida será o endereço atual do usuário
	*/
	
	//Cria um objeto que lida com calculo de rota
	var directionsService = new google.maps.DirectionsService();
	//Cria um objeto que lida com a visualização da rota
	var directionDisplay = new google.maps.DirectionsRenderer(
		{ 
			draggable: true 
		}
	);
	// Avisa pra mostrar no mapa
	directionDisplay.setMap(map);

	//Verifica se vai ter alguma parada no meio do caminho
	if (waypoints != null) {
		waypoints_array = [];
		for (var i = 0; i < waypoints.length; i++) {
		    var address = waypoints[i];
		    if (address !== "") {
		        waypoints_array.push({
			        location: address,
			        stopover: true
		        });
		    }
		}

		//Tem parada pelo caminho
		var request = {
		    origin: originAddress, //Ponto de Origem
			destination: destinationAddress, //Destino final
			waypoints: waypoints_array, //Todos os pontos que irá parar
			optimizeWaypoints: true, //Calcula a rota mais curta entre os pontos
			travelMode: google.maps.DirectionsTravelMode.DRIVING //Diz que a rota vai ser feita de carro
		};

		//Verifica, define e mostra a rota
		directionsService.route(request, function (response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionDisplay.setDirections(response);
			}
		});
	}else{
		//Não há paradas pelo caminho
		var request = {
		    origin: originAddress, //Ponto de Origem
			destination: destinationAddress, //Destino final
			travelMode: google.maps.DirectionsTravelMode.DRIVING //Diz que a rota vai ser feita de carro
		};

		//Verifica, define e mostra a rota
		directionsService.route(request, function (response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionDisplay.setDirections(response);
			}
		});
	}	
}

function createCircle(map, raio, centro){
	alert("era pra funcionar");
	var circle = new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: map,
      center: centro,
      radius: raio
    });
} 
;
