# app/controllers/registrations_controller
class RegistrationsController < Devise::RegistrationsController
  skip_before_filter :authenticate_user!, :only => "reply", :raise => false
  # def facebook
  #   @usuario = Usuario.from_omniauth(request.env["omniauth.auth"])
  #   sign_in_and_redirect @usuario
  # end

  def sign_up_params
     params.require(:usuario).permit(:nome, :data_nascimento, :sexo, :telefone, :email, :senha, :rua, :bairro, :cidade, :estado, :password, :password_confirmation)
  end

  # def account_update_params
  #   #params.require(:usuario).permit(:name, :email, :password, :password_confirmation, :current_password)
  # end
end
