class Update < ActiveRecord::Migration[5.0]
  def change
  	change_table :anuncios do |t|
      t.belongs_to :usuario, foreign_key: true
    end
  end
end
