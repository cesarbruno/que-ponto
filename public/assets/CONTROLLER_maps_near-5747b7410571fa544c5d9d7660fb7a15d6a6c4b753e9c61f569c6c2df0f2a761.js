/*
	near CONTROLLER

	require 'map_utils.js'
*/
/*
	Notes =>
		Bug na hora de tirar os pontos marcados no mapa. ClearMarkerPlace()
		Bug dos ícones. Se algum que for ícone e já estiver na array TIPOS o ícone não carrega.
		Eventos assícronos do infowindow e datalhes do local.
 
*/

var DIV_GOOGLE = [];
var DIV_IBGE = [];
var markers = [];
var PLACES_MARKERS = [];
var CIRCLES = [];
var CENTRO;
var RAIO;
var TIPOS = ['bank'];//['store', 'establishment'];

function getPlacesByClick(map, centro, codigoSencitario = null, nomeBairros = null, POLYGON = null){
	if (codigoSencitario != null && nomeBairros != null && POLYGON != null) {
		PRIVATE_clearMarker(true);
		//var radius = prompt("Digite o raio de alcance: ", "metros");
		RAIO = 800;
		//var tipo = prompt("Diga-nos os tipos", "store");
		CENTRO = centro;
		setStreetView(map, CENTRO); // Mostra o street view
		getPlaces(map, null, codigoSencitario, nomeBairros, POLYGON);//, event.latLng, RAIO, ['store'])

	}else{
		PRIVATE_clearMarker(true);
		//var radius = prompt("Digite o raio de alcance: ", "metros");
		RAIO = 800;
		//var tipo = prompt("Diga-nos os tipos", "store");
		CENTRO = centro;
		setStreetView(map, CENTRO); // Mostra o street view
		getPlaces(map);//, event.latLng, RAIO, ['store'])
	}
	//Limpa os velhos markers
		
	//Cria o envento
	/*google.maps.event.addListener(map, 'click', function(event) {
		//Limpa os velhos markers
		PRIVATE_clearMarker();
		//var radius = prompt("Digite o raio de alcance: ", "metros");
		RAIO = 800;
		//var tipo = prompt("Diga-nos os tipos", "store");
		CENTRO = event.latLng;
		setStreetView(map, CENTRO); // Mostra o street view
		getPlaces(map);//, event.latLng, RAIO, ['store'])
  	});*/
}



function FirstOpen(map, center){
	if (CIRCLES.length == 0) {
    	var circle = new google.maps.Circle({
	      strokeColor: '#FF0000',
	      strokeOpacity: 0.8,
	      strokeWeight: 2,
	      fillColor: '#FF0000',
	      fillOpacity: 0.35,
	      map: map,
	      center: center,
	      radius: 800,
	      editable: true
	    });
	    google.maps.event.addListener(circle, 'radius_changed', function(event){
	    	RAIO = this.getRadius();
	    	if (type == null) {
	    		realizarBusca(map, null, codigoSencitario, nomeBairros, POLYGON);
	    	}else{
	    		realizarBusca(map, type, codigoSencitario, nomeBairros, POLYGON);
	    	}
	    	
	    	
	    });
    	CIRCLES.push(circle);
    }  
    var infowindow = new google.maps.InfoWindow();
    var type = null;
    if (type != null) {
    	//Centraliza o mapa no centro da circunferência
		map.setCenter(center);
		var request = {
		    location: center, //Centro da circunferência
		    radius: 800, //Parâmetro raio
		    types: TIPOS //Parâmetro tipo
		  };
		  //Cria um objeto que lida com Places
		  service = new google.maps.places.PlacesService(map);

		  //Faz uma pesquisa com os parâmetro citados em request
		  service.nearbySearch(request, callback);

		  //Cria um objeto que lida com pequenas janelas de notificações encima do apontadro (marker)
		  //infowindow = new google.maps.InfoWindow();

		function callback(results, status) {
		  if (status == google.maps.places.PlacesServiceStatus.OK) {
		    for (var i = 0; i < results.length; i++) {
		    	if(markers.length == 0){
		    		var icon = GetIcon(type);
			    	if (icon != null) {
			    		PRIVATE_createMarker(map, infowindow, results[i], icon);
			    	}else{
			    		PRIVATE_createMarker(map, infowindow, results[i]);
			    	}
		    	}else{
		    		for (var j = 0; j < markers.length; j++) {
			    		var lat = markers[j].getPosition().lat();
			    		var lng = markers[j].getPosition().lng();
			    		var latLng = new google.maps.LatLng(lat, lng);
			    		if (latLng == results[i].geometry.location) {
			    			continue;
			    		}else{
			    			var icon = GetIcon(type);
			    			if (icon != null) {
			    				PRIVATE_createMarker(map, infowindow, results[i], icon);
			    				break;
			    			}else{
			    				PRIVATE_createMarker(map, infowindow, results[i]);
			    				break;
			    			}
			    		}
			    	}
		    	}		    		      
		    }
		  }
		}
    	
    }else{
    	//Centraliza o mapa no centro da circunferência
		map.setCenter(center);
		var request = {
		    location: center, //Centro da circunferência
		    radius: 800, //Parâmetro raio
		    types: TIPOS //Parâmetro tipo
		  };
		  //Cria um objeto que lida com Places
		  service = new google.maps.places.PlacesService(map);

		  //Faz uma pesquisa com os parâmetro citados em request
		  service.nearbySearch(request, callback);

		  //Cria um objeto que lida com pequenas janelas de notificações encima do apontadro (marker)
		  //infowindow = new google.maps.InfoWindow();

		function callback(results, status) {
		  if (status == google.maps.places.PlacesServiceStatus.OK) {
		    for (var i = 0; i < results.length; i++) {
		      	PRIVATE_createMarker(map, infowindow, results[i]);	      
		    }
		  }
		}
    }
}




function getPlaces(map, type = null, codigoSencitario = null, nomeBairros = null, POLYGON = null){//, centro, raio, tipo){
	/*
		Este método retorna todos os locais de uma área (definido por raio a partir de centro) e dos tipos descritos na array tipo

		map    => object (google.maps.Map)
		centro => object (google.maps.LatLng)
		raio   => intenger
		tipo   => array (string)
	*/
	//raio = parseInt(raio);
	//Cria um circulo no mapa
	//Previne de criar vários círculos
    if (CIRCLES.length == 0) {
    	var circle = new google.maps.Circle({
	      strokeColor: '#FF0000',
	      strokeOpacity: 0.8,
	      strokeWeight: 2,
	      fillColor: '#FF0000',
	      fillOpacity: 0.35,
	      map: map,
	      center: CENTRO,
	      radius: RAIO,
	      editable: true
	    });
	    google.maps.event.addListener(circle, 'radius_changed', function(event){
	    	RAIO = this.getRadius();
	    	if (type == null) {
	    		realizarBusca(map, null, codigoSencitario, nomeBairros, POLYGON);
	    	}else{
	    		realizarBusca(map, type, codigoSencitario, nomeBairros, POLYGON);
	    	}
	    	
	    	
	    });
    	CIRCLES.push(circle);
    }
    GetCodigoSencitario(codigoSencitario, nomeBairros, POLYGON);
    var infowindow = new google.maps.InfoWindow();
    if (type != null) {
    	//Centraliza o mapa no centro da circunferência
		map.setCenter(CENTRO);
		var request = {
		    location: CENTRO, //Centro da circunferência
		    radius: RAIO, //Parâmetro raio
		    types: TIPOS //Parâmetro tipo
		  };
		  //Cria um objeto que lida com Places
		  service = new google.maps.places.PlacesService(map);

		  //Faz uma pesquisa com os parâmetro citados em request
		  service.nearbySearch(request, callback);

		  //Cria um objeto que lida com pequenas janelas de notificações encima do apontadro (marker)
		  //infowindow = new google.maps.InfoWindow();

		function callback(results, status) {
		  if (status == google.maps.places.PlacesServiceStatus.OK) {
		    for (var i = 0; i < results.length; i++) {
		    	if(markers.length == 0){
		    		var icon = GetIcon(type);
			    	if (icon != null) {
			    		PRIVATE_createMarker(map, infowindow, results[i], icon);
			    	}else{
			    		PRIVATE_createMarker(map, infowindow, results[i]);
			    	}
		    	}else{
		    		for (var j = 0; j < markers.length; j++) {
			    		var lat = markers[j].getPosition().lat();
			    		var lng = markers[j].getPosition().lng();
			    		var latLng = new google.maps.LatLng(lat, lng);
			    		if (latLng == results[i].geometry.location) {
			    			continue;
			    		}else{
			    			var icon = GetIcon(type);
			    			if (icon != null) {
			    				PRIVATE_createMarker(map, infowindow, results[i], icon);
			    				break;
			    			}else{
			    				PRIVATE_createMarker(map, infowindow, results[i]);
			    				break;
			    			}
			    		}
			    	}
		    	}		    		      
		    }
		  }
		}
    	
    }else{
    	//Centraliza o mapa no centro da circunferência
		map.setCenter(CENTRO);
		var request = {
		    location: CENTRO, //Centro da circunferência
		    radius: RAIO, //Parâmetro raio
		    types: TIPOS //Parâmetro tipo
		  };
		  //Cria um objeto que lida com Places
		  service = new google.maps.places.PlacesService(map);

		  //Faz uma pesquisa com os parâmetro citados em request
		  service.nearbySearch(request, callback);

		  //Cria um objeto que lida com pequenas janelas de notificações encima do apontadro (marker)
		  //infowindow = new google.maps.InfoWindow();

		function callback(results, status) {
		  if (status == google.maps.places.PlacesServiceStatus.OK) {
		    for (var i = 0; i < results.length; i++) {
		      	PRIVATE_createMarker(map, infowindow, results[i]);	      
		    }
		  }
		}
    }
}

function realizarBusca(map, type = null, codigoSencitario = null, nomeBairros = null, POLYGON = null){
	PRIVATE_clearMarker();
	GetCodigoSencitario(codigoSencitario, nomeBairros, POLYGON);
	console.log(TIPOS);
	var infowindow = new google.maps.InfoWindow();
    if (type != null && TIPOS.length > 0) {
    	//Centraliza o mapa no centro da circunferência
		map.setCenter(CENTRO);
		var request = {
		    location: CENTRO, //Centro da circunferência
		    radius: RAIO, //Parâmetro raio
		    types: TIPOS //Parâmetro tipo
		  };
		  //Cria um objeto que lida com Places
		  service = new google.maps.places.PlacesService(map);

		  //Faz uma pesquisa com os parâmetro citados em request
		  service.nearbySearch(request, callback);

		  //Cria um objeto que lida com pequenas janelas de notificações encima do apontadro (marker)
		  //infowindow = new google.maps.InfoWindow();

		function callback(results, status) {
		  if (status == google.maps.places.PlacesServiceStatus.OK) {
		    for (var i = 0; i < results.length; i++) {
		    	if(markers.length == 0){
		    		var icon = GetIcon(type);
			    	if (icon != null) {
			    		PRIVATE_createMarker(map, infowindow, results[i], icon);
			    	}else{
			    		PRIVATE_createMarker(map, infowindow, results[i]);
			    	}
		    	}else{
		    		for (var j = 0; j < markers.length; j++) {
			    		var lat = markers[j].getPosition().lat();
			    		var lng = markers[j].getPosition().lng();
			    		var latLng = new google.maps.LatLng(lat, lng);
			    		if (latLng == results[i].geometry.location) {
			    			continue;
			    		}else{
			    			var icon = GetIcon(type);
			    			if (icon != null) {
			    				PRIVATE_createMarker(map, infowindow, results[i], icon);
			    				break;
			    			}else{
			    				PRIVATE_createMarker(map, infowindow, results[i]);
			    				break;
			    			}
			    		}
			    	}
		    	}		    		      
		    }
		  }
		}
    	
    }else if(TIPOS.length > 0){
    	//Centraliza o mapa no centro da circunferência
		map.setCenter(CENTRO);
		var request = {
		    location: CENTRO, //Centro da circunferência
		    radius: RAIO, //Parâmetro raio
		    types: TIPOS //Parâmetro tipo
		  };
		  //Cria um objeto que lida com Places
		  service = new google.maps.places.PlacesService(map);

		  //Faz uma pesquisa com os parâmetro citados em request
		  service.nearbySearch(request, callback);

		  //Cria um objeto que lida com pequenas janelas de notificações encima do apontadro (marker)
		  //infowindow = new google.maps.InfoWindow();

		function callback(results, status) {
		  if (status == google.maps.places.PlacesServiceStatus.OK) {
		    for (var i = 0; i < results.length; i++) {
		      	PRIVATE_createMarker(map, infowindow, results[i]);	      
		    }
		  }
		}
    }
}

function PRIVATE_createMarker(map, infowindow, place, iconImg = null) {
	/*

		map   => object (google.maps.Map)
		place => object (google.maps.places)

	*/
	//PRIVATE_clearMarker();
	var marker;
	if (iconImg != null) {
		//Cria um apontador (marker) no mapa "map" com localização do place
		var iconResized = {
		    url: iconImg, // url
		    scaledSize: new google.maps.Size(50, 50)//, // scaled size
		    //origin: new google.maps.Point(0,0), // origin
		    //anchor: new google.maps.Point(0, 0) // anchor
		};
		marker = new google.maps.Marker({
			//map: map,
			position: place.geometry.location,
			icon: iconResized
		});
	}else{
		//Cria um apontador (marker) no mapa "map" com localização do place
		marker = new google.maps.Marker({
			//map: map,
			position: place.geometry.location
		});
	}
	marker.setMap(map);
	//Insere o marker na array markers
	PLACES_MARKERS.push(place);
	markers.push(marker);

	
	DIV_GOOGLE.push(place);
	GetDetails(place);
	
	
	/*if (DIV_GOOGLE.length > 0) {
		DIV_GOOGLE.forEach(function(div){
			if (div != place.name) {
				DIV_GOOGLE.push(place.name);
				GetDetails(place);
			}
		});
	}else{
		DIV_GOOGLE.push(place.name);
		GetDetails(place);
	}*/
	
	
	//Ao clicar mostra o place.name
	/*google.maps.event.addListener(marker, 'click', function() {
		var tipos = translateTypes(place.types);

		infowindow.setContent(place.name + "<br>" + tipos + "<br>" + "<button id=\"detail\">Detalhes</button>");
		infowindow.open(map, this);
		document.getElementById("detail").addEventListener('click', function(){
			GetDetails(place);
		});

	});*/
}

function GetDetails(place){
	var container = document.createElement('div');
		var title = document.createElement('h1');
		var type = document.createElement('p');
		var location = document.createElement('p');

		//document.body.appendChild(container);
		document.getElementById("google_info").appendChild(container);
		container.appendChild(title);
		container.appendChild(type);
		container.appendChild(location);

		var tipos = translateTypes(place.types);

		title.innerHTML = place.name;
		type.innerHTML = "Tipo de estabelecimento: \n" + tipos;//place.types;
		location.innerHTML = "Endereço: \n" + place.vicinity;
}

function PRIVATE_clearMarker(cleanCircle = false){
	// Limpa as marcas antigas.
	if (cleanCircle){
		CIRCLES.forEach(function(circle){
			circle.setMap(null)
		});
		CIRCLES = [];
	}
	
	DIV_GOOGLE.forEach(function(){
		var c = document.getElementById("google_info").childNodes;
		for (var i = 0; i < c.length; i++) {
			if (c[i].tagName == 'DIV') {
				c[i].style.display = "none";
			}
		}		
	});
	DIV_GOOGLE = [];
	DIV_IBGE.forEach(function(){
		var c = document.getElementById("ibge_info").childNodes;
		for (var i = 0; i < c.length; i++) {
			if (c[i].tagName == 'DIV') {
				c[i].style.display = "none";
			}
		}
	});
	DIV_IBGE = [];
	markers.forEach(function(marker) {
		marker.setMap(null);
	});
	markers = [];
	PLACES_MARKERS = [];
}

function ClearMarkerPlace(tipo){
	for (var i = 0; i < PLACES_MARKERS.length; i++) {
		var types = PLACES_MARKERS[i].types;
		for (var j = 0; j < types.length; j++) {
			if(types[j] == tipo){
				markers[i].setMap(null);
				markers.splice(i, 1);
				PLACES_MARKERS.splice(i, 1);
				//alert("Eliminado o index: " + i);
				break;
			}
		}
	}
}

function ClickNosTipos(map, codigoSencitario, nomeBairros, POLYGON){

	document.getElementById('bank').onclick = function(){
		for (var i = 0; i < TIPOS.length; i++) {
			console.log(TIPOS);
			if(TIPOS[i] == 'bank'){
				if (TIPOS.length == 1) {
					alert("Você não pode deixar vazio");
					document.getElementById('bank').checked = true;
					break;
				}
				ClearMarkerPlace("bank");
				TIPOS.splice(i, 1);
				realizarBusca(map, null, codigoSencitario, nomeBairros, POLYGON);
				break;
			}else if(i == TIPOS.length - 1){
				TIPOS.push('bank');	
				realizarBusca(map, 'bank', codigoSencitario, nomeBairros, POLYGON);			
				// getPlacesByClick(map, CENTRO);
				break;
			}
		}
		// getPlacesByClick(map, CENTRO);
	}
	document.getElementById('airport').onclick = function(){
		for (var i = 0; i < TIPOS.length; i++) {
			console.log(TIPOS);
			if(TIPOS[i] == 'airport'){
				if (TIPOS.length == 1) {
					alert("Você não pode deixar vazio");
					document.getElementById('airport').checked = true;
					break;
				}
				ClearMarkerPlace("airport");
				TIPOS.splice(i, 1);
				realizarBusca(map, null, codigoSencitario, nomeBairros, POLYGON);
				break;
			}else if(i == TIPOS.length - 1){
				TIPOS.push('airport');
				realizarBusca(map, 'airport', codigoSencitario, nomeBairros, POLYGON);
				// getPlacesByClick(map, CENTRO);
				break;
			}
		}
		//getPlaces(map, 'airport');
	}
	document.getElementById('subway_station').onclick = function(){
		for (var i = 0; i < TIPOS.length; i++) {
			console.log(TIPOS);
			if(TIPOS[i] == 'subway_station'){
				if (TIPOS.length == 1) {
					alert("Você não pode deixar vazio");
					document.getElementById('subway_station').checked = true;
					break;
				}
				ClearMarkerPlace("subway_station");
				TIPOS.splice(i, 1);
				realizarBusca(map, null, codigoSencitario, nomeBairros, POLYGON);
				break;
			}else if(i == TIPOS.length - 1){
				TIPOS.push('subway_station');
				realizarBusca(map, 'subway_station', codigoSencitario, nomeBairros, POLYGON);
				// getPlacesByClick(map, CENTRO);
				break;
			}
		}
		//getPlaces(map, 'subway_station');
	}
	document.getElementById('bus_station').onclick = function(){
		for (var i = 0; i < TIPOS.length; i++) {
			console.log(TIPOS);
			if(TIPOS[i] == 'bus_station'){
				if (TIPOS.length == 1) {
					alert("Você não pode deixar vazio");
					document.getElementById('bus_station').checked = true;
					break;
				}
				ClearMarkerPlace("bus_station");
				TIPOS.splice(i, 1);
				realizarBusca(map, null, codigoSencitario, nomeBairros, POLYGON);
				break;
			}else if(i == TIPOS.length - 1){
				TIPOS.push('bus_station');
				realizarBusca(map, 'bus_station', codigoSencitario, nomeBairros, POLYGON);
				// getPlacesByClick(map, CENTRO);
				break;
			}
		}
		//getPlaces(map, 'bus_station');
	}
	document.getElementById('taxi_stand').onclick = function(){
		for (var i = 0; i < TIPOS.length; i++) {
			console.log(TIPOS);
			if(TIPOS[i] == 'taxi_stand'){
				if (TIPOS.length == 1) {
					alert("Você não pode deixar vazio");
					document.getElementById('taxi_stand').checked = true;
					break;
				}
				ClearMarkerPlace("taxi_stand");
				TIPOS.splice(i, 1);
				realizarBusca(map, null, codigoSencitario, nomeBairros, POLYGON);
				break;
			}else if(i == TIPOS.length - 1){
				TIPOS.push('taxi_stand');
				realizarBusca(map, 'taxi_stand', codigoSencitario, nomeBairros, POLYGON);
				// getPlacesByClick(map, CENTRO);
				break;
			}
		}
		//getPlaces(map, 'taxi_stand');
	}
	document.getElementById('atm').onclick = function(){
		for (var i = 0; i < TIPOS.length; i++) {
			console.log(TIPOS);
			if(TIPOS[i] == 'atm'){
				if (TIPOS.length == 1) {
					alert("Você não pode deixar vazio");
					document.getElementById('atm').checked = true;
					break;
				}
				ClearMarkerPlace("atm");
				TIPOS.splice(i, 1);
				realizarBusca(map, null, codigoSencitario, nomeBairros, POLYGON);
				break;
			}else if(i == TIPOS.length - 1){
				TIPOS.push('atm');
				realizarBusca(map, 'atm', codigoSencitario, nomeBairros, POLYGON);
				// getPlacesByClick(map, CENTRO);
				break;
			}
		}
		//getPlaces(map, 'atm');
	}
	document.getElementById('school').onclick = function(){
		for (var i = 0; i < TIPOS.length; i++) {
			console.log(TIPOS);
			if(TIPOS[i] == 'school'){
				if (TIPOS.length == 1) {
					alert("Você não pode deixar vazio");
					document.getElementById('school').checked = true;
					break;
				}
				ClearMarkerPlace("school");
				TIPOS.splice(i, 1);
				realizarBusca(map, null, codigoSencitario, nomeBairros, POLYGON);
				break;
			}else if(i == TIPOS.length - 1){
				TIPOS.push('school');
				realizarBusca(map, 'school', codigoSencitario, nomeBairros, POLYGON);
				// getPlacesByClick(map, CENTRO);
				break;
			}
		}
		//getPlaces(map, 'school');
	}
	document.getElementById('hospital').onclick = function(){
		for (var i = 0; i < TIPOS.length; i++) {
			console.log(TIPOS);
			if(TIPOS[i] == 'hospital'){
				if (TIPOS.length == 1) {
					alert("Você não pode deixar vazio");
					document.getElementById('hospital').checked = true;
					break;
				}
				ClearMarkerPlace("hospital");
				TIPOS.splice(i, 1);
				realizarBusca(map, null, codigoSencitario, nomeBairros, POLYGON);
				break;
			}else if(i == TIPOS.length - 1){
				TIPOS.push('hospital');
				realizarBusca(map, 'hospital', codigoSencitario, nomeBairros, POLYGON);
				// getPlacesByClick(map, CENTRO);
				break;
			}
		}
		//getPlaces(map, 'hospital');
	}
	document.getElementById('church').onclick = function(){
		for (var i = 0; i < TIPOS.length; i++) {
			console.log(TIPOS);
			if(TIPOS[i] == 'church'){
				if (TIPOS.length == 1) {
					alert("Você não pode deixar vazio");
					document.getElementById('church').checked = true;
					break;
				}
				ClearMarkerPlace("church");
				TIPOS.splice(i, 1);
				realizarBusca(map, null, codigoSencitario, nomeBairros, POLYGON);
				break;
			}else if(i == TIPOS.length - 1){
				TIPOS.push('church');
				realizarBusca(map, 'church', codigoSencitario, nomeBairros, POLYGON);
				//ResetarQuandoClicar();
				//getPlacesByClick(map, CENTRO);
				break;
			}
		}
		//getPlaces(map, 'church');
	}
	document.getElementById('park').onclick = function(){
		for (var i = 0; i < TIPOS.length; i++) {
			console.log(TIPOS);
			if(TIPOS[i] == 'park'){
				if (TIPOS.length == 1) {
					alert("Você não pode deixar vazio");
					document.getElementById('park').checked = true;
					break;
				}
				ClearMarkerPlace("park");
				TIPOS.splice(i, 1);
				realizarBusca(map, null, codigoSencitario, nomeBairros, POLYGON);
				break;
			}else if(i == TIPOS.length - 1){
				TIPOS.push('park');
				realizarBusca(map, 'park', codigoSencitario, nomeBairros, POLYGON);
				// getPlacesByClick(map, CENTRO);
				break;
			}
		}
		//getPlaces(map, 'park');
	}
	document.getElementById('shopping_mall').onclick = function(){
		for (var i = 0; i < TIPOS.length; i++) {
			console.log(TIPOS);
			if(TIPOS[i] == 'shopping_mall'){
				if (TIPOS.length == 1) {
					alert("Você não pode deixar vazio");
					document.getElementById('shopping_mall').checked = true;
					break;
				}
				ClearMarkerPlace("shopping_mall");
				TIPOS.splice(i, 1);
				realizarBusca(map, null, codigoSencitario, nomeBairros, POLYGON);
				break;
			}else if(i == TIPOS.length - 1){
				TIPOS.push('shopping_mall');
				realizarBusca(map, 'shopping_mall', codigoSencitario, nomeBairros, POLYGON);
				// getPlacesByClick(map, CENTRO);
				break;
			}
		}
		//getPlaces(map, 'shopping_mall');
	}
	document.getElementById('university').onclick = function(){
		for (var i = 0; i < TIPOS.length; i++) {
			console.log(TIPOS);
			if(TIPOS[i] == 'university'){
				if (TIPOS.length == 1) {
					alert("Você não pode deixar vazio");
					document.getElementById('university').checked = true;
					break;
				}
				ClearMarkerPlace("university");
				TIPOS.splice(i, 1);
				realizarBusca(map, null, codigoSencitario, nomeBairros, POLYGON);
				break;
			}else if(i == TIPOS.length - 1){
				TIPOS.push('university');
				realizarBusca(map, 'university', codigoSencitario, nomeBairros, POLYGON);
				// getPlacesByClick(map, CENTRO);
				break;
			}
		}
		//getPlaces(map, 'university');
	}
}

function ResetarQuandoClicar(){
	DIV_GOOGLE.forEach(function(){
		var c = document.getElementById("google_info").childNodes;
		for (var i = 0; i < c.length; i++) {
			if (c[i].tagName == 'DIV') {
				c[i].style.display = "none";
			}
		}		
	});
	DIV_GOOGLE = [];
	markers.forEach(function(marker) {
		marker.setMap(null);
	});
}

function GetIcon(type){
	if(type == 'bus_station'){
		return "/assets/maps_icons/bus_station-40aad730befed1dc527551bdf460f9ef377316e26f76ebc8c76c637817c29210.png";
	}else if(type == 'taxi_stand'){
		return "/assets/maps_icons/taxi_stand-155d4568892a2db920bf8a8032e7a944d29c5a784ba7a9cd2211c077cb049620.png";
	}else if(type == 'subway_station'){
		return "/assets/maps_icons/subway_station-dd3f78f2110b5a52169e169556354ec0b1edbe9d39c52b3e1b1a7bb759995f23.png";
	}else{
		return null;
	}
}

function GetCodigoSencitario(codigoSencitario, nomeBairros, POLYGON){
	for (var i = 0; i < POLYGON.length; i++) {
            var paths = POLYGON[i].getPath();
            paths.forEach(function(latlng){
                var LatLng = new google.maps.LatLng(latlng.lat(), latlng.lng());
                var result = CIRCLES[0].getBounds().contains(LatLng);
                if (result && ! DIV_IBGE.includes(codigoSencitario[i])) {
                    var container = document.createElement('div');
                    var title = document.createElement('h1');
                    var bairro = document.createElement('p');
                    var info = document.createElement('p');

                    document.getElementById("ibge_info").appendChild(container);
                    container.appendChild(title);
                    container.appendChild(bairro);
                    container.appendChild(info);

                    title.innerHTML = codigoSencitario[i];
                    bairro.innerHTML = nomeBairros[i];        

                    if (codigoSencitario[i] % 2 == 0) {
                        /*alert(codigoSencitario[index] + "\n" +
                            "Mulheres: 86% \nHomens: 14%\n" +
                            nomeBairros[index]
                            );*/
                        info.innerHTML = "Mulheres: 86% \nHomens: 14%";
                    }else{
                        /*alert(codigoSencitario[index] + "\n" +
                            "Mulheres: 45% \nHomens: 55%\n" +
                            nomeBairros[index]
                            );*/
                        info.innerHTML= "Mulheres: 45% \nHomens: 55%";
                    }
                    DIV_IBGE.push(codigoSencitario[i]);            
                }
            });
        }
}
;
