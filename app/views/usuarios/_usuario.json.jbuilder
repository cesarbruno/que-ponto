json.extract! usuario, :id, :nome, :data_nascimento, :sexo, :telefone, :email, :senha, :rua, :bairro, :cidade, :estado, :created_at, :updated_at
json.url usuario_url(usuario, format: :json)