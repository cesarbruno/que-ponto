json.extract! preco, :id, :preco_aluguel, :preco_venda, :preco_ponto, :preco_condominio, :preco_iptu, :preco_negociavel, :preco_outros, :preco_outros_descricao, :anuncio_id, :created_at, :updated_at
json.url preco_url(preco, format: :json)
