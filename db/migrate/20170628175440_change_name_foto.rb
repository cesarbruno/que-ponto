class ChangeNameFoto < ActiveRecord::Migration[5.0]
  def change
  	change_table :anuncios do |t|
  		t.rename :carga_decarga_fotos, :carga_descarga_fotos
  	end

  end
end
