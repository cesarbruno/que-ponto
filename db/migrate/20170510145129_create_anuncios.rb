class CreateAnuncios < ActiveRecord::Migration[5.0]
  def change
    create_table :anuncios do |t|
      t.string :pretencao
      t.string :tipo_imovel
      t.string :local
      t.integer :qtd_ambientes
      t.integer :qtd_banheiros
      t.decimal :area_util
      t.decimal :area_total
      t.string :nome
      t.text :descricao
      t.string :logradouro
      t.integer :numero
      t.string :bairro
      t.string :cidade
      t.string :estado
      t.string :cep
      t.string :piso
      t.boolean :estacionamento_rua
      t.boolean :estacionamento_privativo
      t.decimal :estacionamento_preco
      t.integer :estacionamento_tempo
      t.integer :estacionamento_qtd_vagas
      t.string :estacionamento_fluxo
      t.text :estacionamento_descricao
      t.boolean :carga_descarga
      t.text :carga_descarga_descricao
      t.decimal :preco_aluguel
      t.decimal :preco_condominio
      t.decimal :preco_iptu
      t.decimal :preco_ponto
      t.boolean :preco_negociavel

      t.timestamps
    end
  end
end
