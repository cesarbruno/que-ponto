/*
	routes CONTROLLER

	require 'map_utils.js'
*/
/*
	Notes =>
		Terminar a parte de montagem de rota
		Comentar e documentar esse arquivo

*/


var FROM; // De onde vai sair
var WAYPOINTS = []; //Por onde vai passar
var WAYPOINTS_PLACE = []; //A ID do lugar salvo em WAYPOINTS
var MARKERS = []; //Marcadores no mapa

function AddWaypoint(map){
	/*
		Esta função adiciona um novo local ao waypoint

		local 	=> object (google.Maps.ltdLng)
	*/
	var geocoder = new google.maps.Geocoder;
	var addWaypoint = function(event){
		geocoder.geocode({'location': event.latLng}, function(results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				if (results[1]) {
					var request = {
						placeId: results[1].place_id
					};

					service = new google.maps.places.PlacesService(map);
					service.getDetails(request, callback);
					function callback(place, status) {
						if (status == google.maps.places.PlacesServiceStatus.OK) {
							var container = document.createElement('div'); // Grande div que contém todo conteudo
							var divName = document.createElement('h1'); //Onde ficará o nome do local
							var divAddress = document.createElement('p'); //Onde vai ficar o conteudo do endereço
							var inputPlaceId = document.createElement('a'); //Cria o elemento que vai guardar a id do
							container.setAttribute("id", place.place_id);
							inputPlaceId.innerHTML = "Remover";
							inputPlaceId.style.cursor = 'pointer';
							inputPlaceId.style.color = 'blue';
							inputPlaceId.onclick = function(){
								RemoveWaypoint(map, place);
							}
							document.body.appendChild(container);
							container.appendChild(inputPlaceId);
							container.appendChild(divName);
							container.appendChild(divAddress);
							if (place.name != undefined) {
							   	divName.innerHTML = place.name;
							}else{
							   	divName.innerHTML = "Lugar " + WAYPOINTS.length;
							}
							divAddress.innerHTML = place.formatted_address;
							//alert(place.formatted_address);

							WAYPOINTS_PLACE.push(place.place_id);
							newMarker(map, place, MARKERS, place.place_id);
						}
					}
				}
			}
		});
		var local = event.latLng;
		//WAYPOINTS.push(local);
		WAYPOINTS.push(local);
	}
	var ASSYNCWaypoint = google.maps.event.addListener(map, "click", addWaypoint);
	return ASSYNCWaypoint;	
}

function RemoveWaypoint(map, place){
	/*
		Esta função remove um waypoint do array

		local 	=> object (google.Maps.ltdLng)
		map => object (google.maps.Map)
	*/
	for (var i = 0; i < WAYPOINTS_PLACE.length; i++) {
		if (WAYPOINTS_PLACE[i] == place.place_id) {
			document.getElementById(place.place_id).style.display = 'none';
			WAYPOINTS_PLACE.splice(i, 1);
			WAYPOINTS.splice(i, 1);
			removeMarker(MARKERS, place.place_id);
		}
	}
}

function SetRoute(map){
	/*
		Esta funcção seta no mapa a melhor rota

		map => object (google.maps.Map)
	*/
	//Cria um objeto que lida com calculo de rota
	var directionsService = new google.maps.DirectionsService();
	//Cria um objeto que lida com a visualização da rota
	var directionDisplay = new google.maps.DirectionsRenderer(
		{ 
			draggable: true 
		}
	);
	// Avisa pra mostrar no mapa
	directionDisplay.setMap(map);

	//Verifica se vai ter alguma parada no meio do caminho
	if (WAYPOINTS != null) {
		//Tem parada pelo caminho
		var request = {
		    origin: FROM, //Ponto de Origem
			destination: WAYPOINTS[WAYPOINTS.length - 1], //Destino final
			waypoints: WAYPOINTS, //Todos os pontos que irá parar
			optimizeWaypoints: true, //Calcula a rota mais curta entre os pontos
			travelMode: google.maps.DirectionsTravelMode.DRIVING //Diz que a rota vai ser feita de carro
		};

		//Verifica, define e mostra a rota
		directionsService.route(request, function (response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionDisplay.setDirections(response);
			}
		});
	}else{
		//Não há paradas pelo caminho
		var request = {
		    origin: FROM, //Ponto de Origem
			destination: WAYPOINTS[WAYPOINTS.length - 1], //Destino final
			travelMode: google.maps.DirectionsTravelMode.DRIVING //Diz que a rota vai ser feita de carro
		};

		//Verifica, define e mostra a rota
		directionsService.route(request, function (response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionDisplay.setDirections(response);
			}
		});
	}	
}

function ConfirmCurrentLocation(map){
	/*
		Esta função confirma e seta a atual localização do usuário
	*/
	//geolocate();
	var geocoder = new google.maps.Geocoder;
	var confirmarLocalizacao;
  
	geocoder.geocode({'location': map.getCenter()}, function(results, status) {
		if (status === google.maps.GeocoderStatus.OK) {
	    	if (results[1]) {

				var request = {
			  		placeId: results[1].place_id
				};

				service = new google.maps.places.PlacesService(map);
				service.getDetails(request, callback);
				function callback(place, status) {
					if (status == google.maps.places.PlacesServiceStatus.OK) {
			  			confirmarLocalizacao = confirm("Você está em: \n" +
			  				place.formatted_address + "?"
			  			);
						if (confirmarLocalizacao) {
							FROM = map.getCenter();
							ImprimirLocal(map, FROM);
							setRoute(map, FROM, WAYPOINTS[WAYPOINTS.length - 1], WAYPOINTS);
						}else{
							alert("Pesquise e clique no mapa sua atual lozalização.");
							ClickMapa(map);
						}
					}
				}
			} else {
	        	window.alert('Nenhum resultado encontrado');
	      	}
	    } else {
	      window.alert('Algo deu errado: ' + status);
	    }
	});
}

function ClickMapa(map){
	/*
		Esta função auxilia na ConfirmCurrentLocation
	*/
	var SetCenterToGetCenter = function (event){
		var center = event.latLng;
		map.setCenter(event.latLng);
		google.maps.event.removeListener(listener);
		ConfirmCurrentLocation(map, event.latLng);
	}
	var listener = google.maps.event.addListener(map, "click", SetCenterToGetCenter);
}

function ImprimirLocal(map, latLNG) {
  var geocoder = new google.maps.Geocoder;
  
  geocoder.geocode({'location': latLNG}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      if (results[1]) {

		var request = {
		  placeId: results[1].place_id
		};

		service = new google.maps.places.PlacesService(map);
		service.getDetails(request, callback);
		function callback(place, status) {
		  if (status == google.maps.places.PlacesServiceStatus.OK) {
		    document.getElementById('localizacao').style.display = 'block';
        	document.getElementById('localContent').innerHTML = place.formatted_address;
		  }
		}

      } else {
        window.alert('Nenhum resultado encontrado');
      }
    } else {
      window.alert('Algo deu errado: ' + status);
    }
  });
}

function getPlaceDetails(map, request){
	service = new google.maps.places.PlacesService(map);
	service.getDetails(request, callback);

	function callback(place, status) {
		if (status == google.maps.places.PlacesServiceStatus.OK) {
		    return place;
		}
	}
}
;
