class UpdatingLike < ActiveRecord::Migration[5.0]
  def change
  	change_table :likes do |t|
  		t.belongs_to :anuncios, index: true
  	end
  end
end
