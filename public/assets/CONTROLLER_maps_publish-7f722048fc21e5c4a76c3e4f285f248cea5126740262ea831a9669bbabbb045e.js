/*
	publish CONTROLLER

	require 'map_utils.js'
*/
/*
	Notes =>
		Ver quais são realmente os campos necessários para serem preenchidos.

*/

function ClickMapa(map){
	var SetCenterToGetCenter = function (event){
		var center = event.latLng;
		map.setCenter(event.latLng);
		setStreetView(map, event.latLng);
		SetIntoForm(map, event.latLng);
	}
	var listener = google.maps.event.addListener(map, "click", SetCenterToGetCenter);
}

function SetIntoForm(map, location){
	var geocoder = new google.maps.Geocoder;
  
	geocoder.geocode({'location': map.getCenter()}, function(results, status) {
		if (status === google.maps.GeocoderStatus.OK) {
	    	if (results[1]) {

				var request = {
			  		placeId: results[1].place_id
				};

				service = new google.maps.places.PlacesService(map);
				service.getDetails(request, callback);
				function callback(place, status) {
					if (status == google.maps.places.PlacesServiceStatus.OK) {
			  			//document.getElementById('endereco').value = place.formatted_address;
			  			//alert("latitude: " + place.geometry.location.lat() + "\nlongitude: " + place.geometry.location.lng());

			  			/**********************************************************************
			  					Verificar localização com janela de popup e preencher formulário automaticamente
			  			***********************************************************************/
			  			var address = confirm("Você selecionou um ponto em: \n" + place.formatted_address + "?");
			  			if (address) {
			  				document.getElementById('ponto_comercial_latitude').value = place.geometry.location.lat();
			  				document.getElementById('ponto_comercial_longitude').value = place.geometry.location.lng();
			  				var pos = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
			  				var marker = new google.maps.Marker({
								//map: map,
								position: pos
							});
							marker.setMap(map);
			  				//newMarker(map, place, array, place.place_id);
			  				alert("Seleção feita com sucesso, continue preenchendo.");
			  			}else{
			  				return 0;
			  			}
					}
				}
			} else {
	        	window.alert('Nenhum resultado encontrado');
	      	}
	    } else {
	      window.alert('Algo deu errado: ' + status);
	    }
	});
}
;
