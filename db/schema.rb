# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171120051619) do

  create_table "anuncios", force: :cascade do |t|
    t.string  "pretencao"
    t.string  "tipo_imovel"
    t.integer "qtd_ambientes"
    t.integer "qtd_banheiros"
    t.decimal "area_util"
    t.decimal "area_total"
    t.string  "nome"
    t.text    "descricao"
    t.string  "logradouro"
    t.integer "numero"
    t.string  "bairro"
    t.string  "cidade"
    t.string  "estado"
    t.string  "cep"
    t.string  "piso"
    t.boolean "estacionamento_rua"
    t.boolean "estacionamento_privativo"
    t.integer "estacionamento_qtd_vagas"
    t.text    "estacionamento_descricao"
    t.boolean "carga_descarga"
    t.text    "carga_descarga_descricao"
    t.string  "fotos"
    t.string  "video"
    t.boolean "publicar"
    t.string  "carga_descarga_fotos"
    t.string  "codigo"
    t.string  "codigos_censitarios"
    t.boolean "estacionamento_coletivo"
    t.boolean "estacionamento_pago"
    t.boolean "adaptado"
    t.string  "adaptado_descricao"
    t.string  "fluxo_15min"
    t.string  "fluxo_medio_diario"
    t.string  "fluxo_horario_pico"
    t.string  "fluxo_pior_dia"
    t.string  "tipo_negocio"
    t.string  "segmento"
    t.integer "idade_empresa"
    t.string  "motivo_venda"
    t.integer "qtd_funcionarios"
    t.float   "faturamento_bruto"
    t.float   "lucro_medio"
    t.float   "capital_giro"
    t.float   "valor_estoque"
    t.integer "usuario_id"
    t.index ["usuario_id"], name: "index_anuncios_on_usuario_id"
  end

  create_table "chat_rooms", force: :cascade do |t|
    t.string   "titulo"
    t.integer  "usuario_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "usuario2_id"
    t.index ["usuario2_id"], name: "index_chat_rooms_on_usuario2_id"
    t.index ["usuario_id"], name: "index_chat_rooms_on_usuario_id"
  end

  create_table "ibges", force: :cascade do |t|
    t.string  "cod_sencitario"
    t.string  "nome_municipio"
    t.string  "nome_bairro"
    t.float   "renda_municipio"
    t.integer "qtd_total_homens"
    t.integer "qtd_homens_reponsaveis_domicilio"
    t.integer "qtd_homens_00_10"
    t.integer "qtd_homens_11_20"
    t.integer "qtd_homens_21_30"
    t.integer "qtd_homens_31_40"
    t.integer "qtd_homens_41_50"
    t.integer "qtd_homens_51_60"
    t.integer "qtd_homens_61"
    t.integer "renda_homens_A"
    t.integer "renda_homens_B"
    t.integer "renda_homens_C"
    t.integer "renda_homens_D"
    t.integer "renda_homens_E"
    t.integer "qtd_total_mulheres"
    t.integer "qtd_mulheres_reponsaveis_domicilio"
    t.integer "qtd_mulheres_00_10"
    t.integer "qtd_mulheres_11_20"
    t.integer "qtd_mulheres_21_30"
    t.integer "qtd_mulheres_31_40"
    t.integer "qtd_mulheres_41_50"
    t.integer "qtd_mulheres_51_60"
    t.integer "qtd_mulheres_61"
    t.integer "renda_mulheres_A"
    t.integer "renda_mulheres_B"
    t.integer "renda_mulheres_C"
    t.integer "renda_mulheres_D"
    t.integer "renda_mulheres_E"
    t.integer "qtd_domicilios"
    t.integer "qtd_familias"
    t.float   "renda_domicilios"
  end

  create_table "likes", force: :cascade do |t|
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "usuario"
    t.integer  "anuncios_id"
    t.index ["anuncios_id"], name: "index_likes_on_anuncios_id"
  end

  create_table "messages", force: :cascade do |t|
    t.text     "conteudo"
    t.integer  "usuario_id"
    t.integer  "ChatRoom_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["ChatRoom_id"], name: "index_messages_on_ChatRoom_id"
    t.index ["usuario_id"], name: "index_messages_on_usuario_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "usuario_id"
    t.boolean  "visualizado"
    t.string   "conteudo"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["usuario_id"], name: "index_notifications_on_usuario_id"
  end

  create_table "precos", force: :cascade do |t|
    t.float    "preco_aluguel"
    t.float    "preco_venda"
    t.float    "preco_ponto"
    t.float    "preco_condominio"
    t.float    "preco_iptu"
    t.boolean  "preco_negociavel"
    t.float    "preco_outros"
    t.string   "preco_outros_descricao"
    t.integer  "anuncio_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["anuncio_id"], name: "index_precos_on_anuncio_id"
  end

  create_table "usuarios", force: :cascade do |t|
    t.string   "nome"
    t.date     "data_nascimento"
    t.string   "sexo"
    t.string   "telefone"
    t.string   "email"
    t.string   "senha"
    t.string   "rua"
    t.string   "bairro"
    t.string   "cidade"
    t.string   "estado"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.index ["email"], name: "index_usuarios_on_email", unique: true
    t.index ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true
  end

end
