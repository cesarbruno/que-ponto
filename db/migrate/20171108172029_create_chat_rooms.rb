class CreateChatRooms < ActiveRecord::Migration[5.0]
  def change
    create_table :chat_rooms do |t|
      t.string :titulo
      t.belongs_to :usuario, foreign_key: true

      t.timestamps
    end
  end
end
