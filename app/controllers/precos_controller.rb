class PrecosController < ApplicationController
  before_action :set_preco, only: [:show, :edit, :update, :destroy]

  # GET /precos
  # GET /precos.json
  def index
    @precos = Preco.all
  end

  # GET /precos/1
  # GET /precos/1.json
  def show
  end

  # GET /precos/new
  def new
    @preco = Preco.new
  end

  # GET /precos/1/edit
  def edit
  end

  # POST /precos
  # POST /precos.json
  def create
    @preco = Preco.new(preco_params)

    respond_to do |format|
      if @preco.save
        notification_price(Anuncio.find(@preco.anuncio_id))
        format.html { redirect_to anuncio_path(@preco.anuncio_id), notice: 'Preco was successfully created.' }
        format.json { render :show, status: :created, location: @preco }
      else
        format.html { render :new }
        format.json { render json: @preco.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /precos/1
  # PATCH/PUT /precos/1.json
  def update
    respond_to do |format|
      if @preco.update(preco_params)
        format.html { redirect_to anuncio_path(@preco.anuncio_id), notice: 'Preco was successfully updated.' }
        format.json { render :show, status: :ok, location: @preco }
      else
        format.html { render :edit }
        format.json { render json: @preco.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /precos/1
  # DELETE /precos/1.json
  def destroy
    @preco.destroy
    respond_to do |format|
      format.html { redirect_to precos_url, notice: 'Preco was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_preco
      @preco = Preco.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def preco_params
      params.require(:preco).permit(:preco_aluguel, :preco_venda, :preco_ponto, :preco_condominio, :preco_iptu, :preco_negociavel, :preco_outros, :preco_outros_descricao, :anuncio_id)
    end
end
