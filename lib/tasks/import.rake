require 'csv'
namespace :IBGE do
	desc "Importando detalhes da planilha Pessoa11"
	task ImportPessoa11: :environment do
		path = File.join Rails.root, "public/IBGE/PE/Pessoa11_PE.csv"
		Dir.glob(path) do |csv_file|
			CSV.foreach(csv_file, encoding: "ISO8859-1", headers: true, col_sep: ";") do |row|
				# foreach line handle the data
				# vXXX = vXXX + 1
				cod_sencitario = row[0].to_s
				qtd_total_homens = row[3].to_i
				qtd_homens_reponsaveis_domicilio = row[4].to_i
				qtd_homens_00_10 = 0
				for i in 25..45
				   qtd_homens_00_10 = qtd_homens_00_10 + row[i].to_i
				end
				qtd_homens_11_20 = 0
				for i in 46..55
				   qtd_homens_11_20 = qtd_homens_11_20 + row[i].to_i
				end
				qtd_homens_21_30 = 0
				for i in 56..65
				   qtd_homens_21_30 = qtd_homens_21_30 + row[i].to_i
				end
				qtd_homens_31_40 = 0
				for i in 66..75
				   qtd_homens_31_40 = qtd_homens_31_40 + row[i].to_i
				end
				qtd_homens_41_50 = 0
				for i in 76..85
				   qtd_homens_41_50 = qtd_homens_41_50 + row[i].to_i
				end
				qtd_homens_51_60 = 0
				for i in 86..95
				   qtd_homens_51_60 = qtd_homens_51_60 + row[i].to_i
				end
				qtd_homens_61 = 0
				for i in 96..135
				   qtd_homens_61 = qtd_homens_61 + row[i].to_i
				end



				ibge = Ibge.find_by(cod_sencitario: cod_sencitario)
				if ibge.nil?
					Ibge.create(
						cod_sencitario: cod_sencitario,
						qtd_total_homens: qtd_total_homens, 
						qtd_homens_reponsaveis_domicilio: qtd_homens_reponsaveis_domicilio,
						qtd_homens_00_10: qtd_homens_00_10,
						qtd_homens_11_20: qtd_homens_11_20,
						qtd_homens_21_30: qtd_homens_21_30,
						qtd_homens_31_40: qtd_homens_31_40,
						qtd_homens_41_50: qtd_homens_41_50,
						qtd_homens_51_60: qtd_homens_51_60,
						qtd_homens_61: qtd_homens_61
					)
				else
					ibge.update(
						qtd_total_homens: qtd_total_homens, 
						qtd_homens_reponsaveis_domicilio: qtd_homens_reponsaveis_domicilio,
						qtd_homens_00_10: qtd_homens_00_10,
						qtd_homens_11_20: qtd_homens_11_20,
						qtd_homens_21_30: qtd_homens_21_30,
						qtd_homens_31_40: qtd_homens_31_40,
						qtd_homens_41_50: qtd_homens_41_50,
						qtd_homens_51_60: qtd_homens_51_60,
						qtd_homens_61: qtd_homens_61
					)
				end
				# Save the data
				# Model.create(params: value, ...)
			end # end of csv each
		end # end of each path
	end # end of task Pessoa11

	desc "Importando detalhes da planilha Pessoa12"
	task ImportPessoa12: :environment do
		path = File.join Rails.root, "public/IBGE/PE/Pessoa12_PE.csv"
		Dir.glob(path) do |csv_file|
			CSV.foreach(csv_file, encoding: "ISO8859-1", headers: true, col_sep: ";") do |row|
				# foreach line handle the data
				# vXXX = vXXX + 1
				cod_sencitario = row[0].to_s
				qtd_total_homens = row[3].to_i
				qtd_homens_reponsaveis_domicilio = row[4].to_i
				qtd_homens_00_10 = 0
				for i in 25..45
				   qtd_homens_00_10 = qtd_homens_00_10 + row[i].to_i
				end
				qtd_homens_11_20 = 0
				for i in 46..55
				   qtd_homens_11_20 = qtd_homens_11_20 + row[i].to_i
				end
				qtd_homens_21_30 = 0
				for i in 56..65
				   qtd_homens_21_30 = qtd_homens_21_30 + row[i].to_i
				end
				qtd_homens_31_40 = 0
				for i in 66..75
				   qtd_homens_31_40 = qtd_homens_31_40 + row[i].to_i
				end
				qtd_homens_41_50 = 0
				for i in 76..85
				   qtd_homens_41_50 = qtd_homens_41_50 + row[i].to_i
				end
				qtd_homens_51_60 = 0
				for i in 86..95
				   qtd_homens_51_60 = qtd_homens_51_60 + row[i].to_i
				end
				qtd_homens_61 = 0
				for i in 96..135
				   qtd_homens_61 = qtd_homens_61 + row[i].to_i
				end



				ibge = Ibge.find_by(cod_sencitario: cod_sencitario)
				if ibge.nil?
					Ibge.create(
						cod_sencitario: cod_sencitario,
						qtd_total_mulheres: qtd_total_homens, 
						qtd_mulheres_reponsaveis_domicilio: qtd_homens_reponsaveis_domicilio,
						qtd_mulheres_00_10: qtd_homens_00_10,
						qtd_mulheres_11_20: qtd_homens_11_20,
						qtd_mulheres_21_30: qtd_homens_21_30,
						qtd_mulheres_31_40: qtd_homens_31_40,
						qtd_mulheres_41_50: qtd_homens_41_50,
						qtd_mulheres_51_60: qtd_homens_51_60,
						qtd_mulheres_61: qtd_homens_61
					)
				else
					ibge.update(
						qtd_total_mulheres: qtd_total_homens, 
						qtd_mulheres_reponsaveis_domicilio: qtd_homens_reponsaveis_domicilio,
						qtd_mulheres_00_10: qtd_homens_00_10,
						qtd_mulheres_11_20: qtd_homens_11_20,
						qtd_mulheres_21_30: qtd_homens_21_30,
						qtd_mulheres_31_40: qtd_homens_31_40,
						qtd_mulheres_41_50: qtd_homens_41_50,
						qtd_mulheres_51_60: qtd_homens_51_60,
						qtd_mulheres_61: qtd_homens_61
					)
				end
				# Save the data
				# Model.create(params: value, ...)
			end # end of csv each
		end # end of each path
	end # end of task Pessoa12


	desc "Importando detalhes da planilha de PessoaRenda"
	task ImportPessoaRenda: :environment do
		path = File.join Rails.root, "public/IBGE/PE/PessoaRenda_PE.csv"
		Dir.glob(path) do |csv_file|
			CSV.foreach(csv_file, encoding: "ISO8859-1", headers: true, col_sep: ";") do |row|
				# foreach line handle the data
				# vXXX = vXXX + 1
				cod_sencitario = row[0].to_s
				renda_homens_A = row[98].to_i
				renda_homens_B = (row[96] + row[97]).to_i
				renda_homens_C = (row[94] + row[95]).to_i
				renda_homens_D = (row[92] + row[93]).to_i
				renda_homens_E = (row[90] + row[91]).to_i

				renda_mulheres_A = row[120].to_i
				renda_mulheres_B = (row[118] + row[119]).to_i
				renda_mulheres_C = (row[116] + row[117]).to_i
				renda_mulheres_D = (row[114] + row[115]).to_i
				renda_mulheres_E = (row[112] + row[113]).to_i



				ibge = Ibge.find_by(cod_sencitario: cod_sencitario)
				if ibge.nil?
					Ibge.create(
						cod_sencitario: cod_sencitario,
						renda_homens_A: renda_homens_A,
						renda_homens_B: renda_homens_B,
						renda_homens_C: renda_homens_C,
						renda_homens_D: renda_homens_D,
						renda_homens_E: renda_homens_E,
						renda_mulheres_A: renda_mulheres_A,
						renda_mulheres_B: renda_mulheres_B,
						renda_mulheres_C: renda_mulheres_C,
						renda_mulheres_D: renda_mulheres_D,
						renda_mulheres_E: renda_mulheres_E
					)
				else
					ibge.update(
						renda_homens_A: renda_homens_A,
						renda_homens_B: renda_homens_B,
						renda_homens_C: renda_homens_C,
						renda_homens_D: renda_homens_D,
						renda_homens_E: renda_homens_E,
						renda_mulheres_A: renda_mulheres_A,
						renda_mulheres_B: renda_mulheres_B,
						renda_mulheres_C: renda_mulheres_C,
						renda_mulheres_D: renda_mulheres_D,
						renda_mulheres_E: renda_mulheres_E
					)
				end
				# Save the data
				# Model.create(params: value, ...)
			end # end of csv each
		end # end of each path
	end # end of task PessoaRenda


	desc "Importando detalhes da planilha Domicilio01"
	task ImportDomicilio01: :environment do
		path = File.join Rails.root, "public/IBGE/PE/Domicilio01_PE.csv"
		Dir.glob(path) do |csv_file|
			CSV.foreach(csv_file, encoding: "ISO8859-1", headers: true, col_sep: ";") do |row|
				# foreach line handle the data
				# vXXX = vXXX + 1
				cod_sencitario = row[0].to_s
				qtd_domicilios = row[3].to_i
				qtd_familias = row[3].to_i - (row[88].to_i - row[69].to_i)



				ibge = Ibge.find_by(cod_sencitario: cod_sencitario)
				if ibge.nil?
					Ibge.create(cod_sencitario: cod_sencitario, qtd_domicilios: qtd_domicilios, qtd_familias: qtd_familias)
				else
					ibge.update(qtd_domicilios: qtd_domicilios, qtd_familias: qtd_familias)
				end
				# Save the data
				# Model.create(params: value, ...)
			end # end of csv each
		end # end of each path
	end # end of task Domicilio01


	desc "Importando detalhes da planilha Domicilio01"
	task ImportDomicilioRenda: :environment do
		path = File.join Rails.root, "public/IBGE/PE/DomicilioRenda_PE.csv"
		Dir.glob(path) do |csv_file|
			CSV.foreach(csv_file, encoding: "ISO8859-1", headers: true, col_sep: ";") do |row|
				# foreach line handle the data
				# vXXX = vXXX + 1
				cod_sencitario = row[0].to_s
				renda_domicilios = row[3].to_i

				ibge = Ibge.find_by(cod_sencitario: cod_sencitario)
				if ibge.nil?
					Ibge.create(cod_sencitario: cod_sencitario, renda_domicilios: renda_domicilios)
				else
					ibge.update(renda_domicilios: renda_domicilios)
				end
				# Save the data
				# Model.create(params: value, ...)
			end # end of csv each
		end # end of each path
	end # end of task DomicilioRenda
end