Rails.application.routes.draw do

  resources :notifications
  mount ActionCable.server => '/cable'


  resources :messages
  resources :chat_rooms
  resources :precos
  resources :likes
  #devise_for :usuarios
  # config/routes.rb
  devise_for :usuarios, controllers: { registrations: 'registrations' }
  get 'usuario/conversas', to: "usuarios#conversas"
  get 'usuario/conversa/:chat_room', to: "usuarios#conversa"
  post 'usuario/conversa/:chat_room', to: "usuarios#post_conversa"
  resources :usuarios
  resources :anuncios
  resources :ponto_comercials
  root :to => 'anuncios#index' #seta maps#index para a primeira página do root
  #devise_for :users, :controllers => { :omniauth_callbacks => "callbacks_controller" }
  resources :maps
  # get '/route' => 'maps#maps_routes' #deixa visualizar maps_routes.html.erb
  # get '/publish' => 'ponto_comercials#new'#'maps#maps_publish' #deica visualizar maps_publish.html.erb
  # get '/near' => 'maps#maps_near' #deica visualizar maps_near.html.erb
  # get '/test' => 'maps#test' #teste com KML
  # get '/home' => 'ponto_comercials#home'
  # get '/pesquisa' => 'ponto_comercials#pesquisa'
  # get '/anuncio' => 'ponto_comercials#anuncio'
  # get '/cadastro' => 'ponto_comercials#cadastro'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get 'teste' => "anuncios#test"

  get 'usuario/:id/anuncios' => 'usuarios#anuncios', :as => :usuario_anuncios


  get "busca" => "anuncios#busca", :as => "busca"
  get "cod_sencitario/:id/:cod_sencitario" => "anuncios#cod_sencitario"
end
