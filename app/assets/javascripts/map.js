/*
	To Do =>
		Organizar as informações de retorno ao clicar no local. 
			Link -> https://developers.google.com/maps/documentation/javascript/places?hl=pt-br
		
*/
var markers = [];

function PRIVATE_verifyDestinos(destinos){
	/*
		Essa função organiza e verifica os endereços

		destinos => array
	*/
	var waypoints = [];
	for (var i = 0; i < destinos.length; i++) {
	    var address = destinos[i];
	    if (address !== "") {
	        waypoints.push({
		        location: address,
		        stopover: true
	        });
	    }
	}
	return waypoints;
}

function PRIVATE_routes(map, originAddress, destinationAddress, waypoints = null){
	/*
		Esta função organiza e imprime a rota na tela

		map 				=> object (google.maps.Map)
		originAddress 		=> array (string || object (ltd lng))
		destinationAddress 	=> string || object (ltd lng)
		waypoints 			=> string || object (ltd lng) || null
			Em caso de origem == null então o endereço de partida será o endereço atual do usuário
	*/
	
	//Cria um objeto que lida com calculo de rota
	var directionsService = new google.maps.DirectionsService();
	//Cria um objeto que lida com a visualização da rota
	var directionDisplay = new google.maps.DirectionsRenderer(
		{ 
			draggable: true 
		}
	);
	// Avisa pra mostrar no mapa
	directionDisplay.setMap(map);

	//Verifica se vai ter alguma parada no meio do caminho
	if (waypoints != null) {
		//Tem parada pelo caminho
		var request = {
		    origin: originAddress, //Ponto de Origem
			destination: destinationAddress, //Destino final
			waypoints: waypoints, //Todos os pontos que irá parar
			optimizeWaypoints: true, //Calcula a rota mais curta entre os pontos
			travelMode: google.maps.DirectionsTravelMode.DRIVING //Diz que a rota vai ser feita de carro
		};

		//Verifica, define e mostra a rota
		directionsService.route(request, function (response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionDisplay.setDirections(response);
			}
		});
	}else{
		//Não há paradas pelo caminho
		var request = {
		    origin: originAddress, //Ponto de Origem
			destination: destinationAddress, //Destino final
			travelMode: google.maps.DirectionsTravelMode.DRIVING //Diz que a rota vai ser feita de carro
		};

		//Verifica, define e mostra a rota
		directionsService.route(request, function (response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionDisplay.setDirections(response);
			}
		});
	}	
}

function PRIVATE_clearMarker(){
	// Limpa as marcas antigas.
	markers.forEach(function(marker) {
		marker.setMap(null);
	});
	markers = [];
}

function PRIVATE_createMarker(map, infowindow, place) {
	/*
		Essa função cria marcadores no mapa e diz que ao clicar deve-se mostrar o nome do local

		map   => object (google.maps.Map)
		infowindow => obejct (google.maps.InfoWindow)
		place => object (google.maps.places)

	*/
	//Cria um apontador (marker) no mapa "map" com localização do place
	var marker = new google.maps.Marker({
		map: map,
		position: place.geometry.location
	});
	//Insere o marker na array markers
	markers.push(marker);
	//Ao clicar mostra o place.name
	google.maps.event.addListener(marker, 'click', function() {
		//Abre a janelinha com nome e uma array do tipo de estabelecimento que se encaixa
		infowindow.setContent(
			'<h1>' + place.name + '</h1>' + 
			'<br><p>Tipo: ' + place.types + '</p>' +
			'<br><p>Ranking: ' + place.vicinity + '</p>'
			);
		place = place.getDetails();
		document.getElementById('informacao').style.display = 'block';
		document.getElementById('name').innerHTML = place.name;
		document.getElementById('types').innerHTML = place.types;
		document.getElementById('vinicity').innerHTML = place.vicinity;
		document.getElementById('rating').innerHTML = place.rating;
		document.getElementById('reviews').innerHTML = place.reviews[0].text;
		infowindow.open(map, this);
	});
}

function PRIVATE_searchBox(map, searchBox){
	/*
		Esta função faz com que o search box do mapa realmente funcione

		map => object (google.maps.Map)
		searchBox = object (google.maps.places.SearchBox)
	*/

	// Avisa e libera o resulta da searchBox no mapa
    map.addListener('bounds_changed', function() {
    	searchBox.setBounds(map.getBounds());
    });
	// Quando o local é alterado no searchBox executa essa função
    searchBox.addListener('places_changed', function() {
	    var places = searchBox.getPlaces();

	    if (places.length == 0) {
	      return;
	    }
	     // Limpa as marcas antigas.
		PRIVATE_clearMarker()

	    // Pega o nome e a localização de cada lugar.
	    var bounds = new google.maps.LatLngBounds();

	    places.forEach(function(place) {
	      var icon = {
	        url: place.icon,
	        size: new google.maps.Size(71, 71),
	        origin: new google.maps.Point(0, 0),
	        anchor: new google.maps.Point(17, 34),
	        scaledSize: new google.maps.Size(25, 25)
	      };

	      // Cria uma marca para cada lugar.
	      markers.push(new google.maps.Marker({
	        map: map,
	        icon: icon,
	        title: place.name,
	        position: place.geometry.location
	      }));


	      if (place.geometry.viewport) {
	        // Only geocodes have viewport.
	        bounds.union(place.geometry.viewport);
	      } else {
	        bounds.extend(place.geometry.location);
	      }
	    });
	    map.fitBounds(bounds);
    });
}

function initMap(){
	/*
		Esta função deve ser chamada toda vez que houver um mapa na página.
		Na ID = "map_canvas";
	*/
	var mapOP = { zoom: 16 };
	map = new google.maps.Map(document.getElementById('map_canvas'), mapOP);


    // Cria a Search Box e linka ao mapa.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);

    // Seta a utilização da caixa de pesquisa
    PRIVATE_searchBox(map, searchBox);
    
	return map;
}

function trafficLayer(map){
	/*
		Esta função libera a visualização do trânsito

		map => object (google.maps.Map)
	*/
	var trafficLayer = new google.maps.TrafficLayer();
  	trafficLayer.setMap(map); 
}

function directions(map, destinos, origem = null){
	/*
		Esta função monta a rota de visitas

		map 	 => object (google.maps.Map)
		destinos => array (string || object (ltd lng))
		origem 	 => string || object (ltd lng) || null
			Em caso de origem == null então o endereço de partida será o endereço atual do usuário
	*/
	var origem = origem;
	if (origem === null) {
		//Seta a localização atual como origem
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function (position) {
				var initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				//origem é igual a localização atual do usuário
				origem = initialLocation;
				//Agora se verifica se há mais de um destino
				if (destinos.constructor === Array) {
					//Há mais de um destino
					//Verifica cada destino e organiza
					var waypoints = PRIVATE_verifyDestinos(destinos);
					//Faz a rota e imprime
					PRIVATE_routes(map, origem, origem, waypoints);

					/*************************Atenção*****************************\
							A origem e destino tem mesma localização, verificar isso
					/**************************************************************/

				}else{
					//Só há um destino
					//Faz a rota e imprime
					PRIVATE_routes(map, origem, destinos);
				}
			});
		}
	}else{
		//Tem uma localização de origem
		//Agora verifica se há mais de um destino
		if (destinos.constructor === Array) {
			//Há mais de um destino
			//Verifica cada destino e organiza
			var waypoints = PRIVATE_verifyDestinos(destinos);
			//Faz a rota e imprime
			PRIVATE_routes(map, origem, origem, waypoints);

			/*************************Atenção*****************************\
					A origem e destino tem mesma localização, verificar isso
			/**************************************************************/

		}else{
			//Só há um destino
			//Faz a rota e imprime
			PRIVATE_routes(map, origem, destinos);
		}
	}
}

function getPlacesByClick(map){
	//Cria o envento
	google.maps.event.addListener(map, 'click', function(event) {
		//Limpa os velhos markers
		PRIVATE_clearMarker(),
		getPlaces(map, event.latLng, 500, ['store', 'dentist', 'school'])
  	});
}

function getPlaces(map, centro, raio, tipo){
	/*
		Este método retorna todos os locais de uma área (definido por raio a partir de centro) e dos tipos descritos na array tipo

		map    => object (google.maps.Map)
		centro => object (google.maps.LatLng)
		raio   => intenger
		tipo   => array (string)
	*/

	//Centraliza o mapa no centro da circunferência
	map.setCenter(centro);
	var request = {
	    location: centro, //Centro da circunferência
	    radius: raio, //Parâmetro raio
	    types: tipo //Parâmetro tipo
	  };
	  //Cria um objeto que lida com Places
	  service = new google.maps.places.PlacesService(map);

	  //Faz uma pesquisa com os parâmetro citados em request
	  service.nearbySearch(request, callback);

	  //Cria um objeto que lida com pequenas janelas de notificações encima do apontadro (marker)
	  infowindow = new google.maps.InfoWindow();

	function callback(results, status) {
	  if (status == google.maps.places.PlacesServiceStatus.OK) {
	    for (var i = 0; i < results.length; i++) {
	      	PRIVATE_createMarker(map, infowindow, results[i]);	      
	    }
	  }
	}
}