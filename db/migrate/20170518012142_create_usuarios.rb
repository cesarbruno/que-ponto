class CreateUsuarios < ActiveRecord::Migration[5.0]
  def change
    create_table :usuarios do |t|
      t.string :nome
      t.date :data_nascimento
      t.string :sexo
      t.string :telefone
      t.string :email
      t.string :senha
      t.string :rua
      t.string :bairro
      t.string :cidade
      t.string :estado

      t.timestamps
    end
  end
end
