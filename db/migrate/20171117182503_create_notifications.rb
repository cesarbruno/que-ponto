class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.belongs_to :usuario, foreign_key: true
      t.boolean :visualizado
      t.string :conteudo

      t.timestamps
    end
  end
end
