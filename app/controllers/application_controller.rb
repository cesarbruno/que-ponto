class ApplicationController < ActionController::Base
  #protect_from_forgery with: :exception
  #before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :login_check

def login_check
  @logged_in = signed_in?
end
def after_sign_in_path_for(resource)
    root_path #request.env['omniauth.origin'] || stored_location_for(resource) || root_path
end
def notification_like(anuncio)
	notification_content = "#{current_usuario.nome} de #{current_usuario.cidade} acabou de favoritar seu ponto #{anuncio.nome}"
	Notification.create!(usuario_id: anuncio.usuario_id, visualizado: false, conteudo: notification_content)
end

def notification_price(anuncio)
	notification_content = "O ponto #{anuncio.nome} de #{anuncio.cidade} acabou de alterar o valor. <a href=\"/anuncios/#{anuncio.id}\">Anuncio</a> "
	likes = Like.where(anuncios_id: anuncio.id)
	for like in likes
		Notification.create!(usuario_id: like.usuario, visualizado: false, conteudo: notification_content)
	end	
end
	protected
	 
	def configure_permitted_parameters
	  devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name])
	  devise_parameter_sanitizer.permit(:account_update, keys: [:email, :password, :password_confirmation, :current_password])
	end
end
