
	function initMap(inicial = null) {
		/*
			Editar para que possa ser aberto o mapa na posição desejada
		*/
	  map = new google.maps.Map(document.getElementById('map_canvas'), {
	    center: {lat: -34.397, lng: 150.644},
	    //Caso não possa pegar a localização ele centraliza no ponto definido acima
	    zoom: 14
	  });
	  //Pega a localização do usuário
	  if (navigator.geolocation) {
	     navigator.geolocation.getCurrentPosition(function (position) {
	         initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	         map.setCenter(initialLocation);
	     });
	 }
	}


	function directions_MaisDeUma(destinos){
		var waypoints = [];
		for (var i = 0; i < destinos.length; i++) {
		    var address = destinos[i];
		    if (address !== "") {
		        waypoints.push({
		        location: address,
		        stopover: true
		        });
		    }
		}
		return waypoints;
	}



	function directions(destino, origem = null){
		if (origem != null) {
			if (destino.constructor === Array) {
				var directionsService = new google.maps.DirectionsService();

				var renderOptions = { draggable: true };
				var directionDisplay = new google.maps.DirectionsRenderer(renderOptions);
				map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);
				directionDisplay.setMap(map);

				waypoints = directions_MaisDeUma(destino);

				var originAddress = origem;
				var destinationAddress = origem;
				var request = {
				            origin: originAddress,
				            destination: destinationAddress,
				            waypoints: waypoints, //todas paradas
				            optimizeWaypoints: true, //Organiza o ponto mais curto entre os pontos
				            travelMode: google.maps.DirectionsTravelMode.DRIVING
				        };

				directionsService.route(request, function (response, status) {
				    if (status == google.maps.DirectionsStatus.OK) {
				        directionDisplay.setDirections(response);
				    }
				});
			}else{
				/*
				*	Mostra direção com origem selecionada
				*/
				var directionsService = new google.maps.DirectionsService();
				var directionsDisplay = new google.maps.DirectionsRenderer();
				var mapOptions = {
				    zoom: 14
				  }

				map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);

				directionsDisplay.setMap(map);

				var request = {
				  origin: origem,
				  destination: destino,
				  travelMode: google.maps.DirectionsTravelMode.DRIVING
				};
				directionsService.route(request, function(response, status) {
				  //Check if request is successful.
				  if (status == google.maps.DirectionsStatus.OK) {
				    console.log(status);
				    directionsDisplay.setDirections(response); //Display the directions result
				  }
				});
			}
			
		}else{
			/*
			*	Mostra direção com origem atual
			*/
			if (navigator.geolocation) {
			     navigator.geolocation.getCurrentPosition(function (position) {
			        var initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			        var directionsService = new google.maps.DirectionsService();
					var directionsDisplay = new google.maps.DirectionsRenderer();

					map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);

					directionsDisplay.setMap(map);

					var request = {
					  origin: initialLocation,
					  destination: destino,
					  travelMode: google.maps.DirectionsTravelMode.DRIVING
					};
					directionsService.route(request, function(response, status) {
					  //Check if request is successful.
					  if (status == google.maps.DirectionsStatus.OK) {
					    console.log(status);
					    directionsDisplay.setDirections(response); //Display the directions result
					  }
					});
			     });
			}else{
				return;
			}
		}
	}
;
