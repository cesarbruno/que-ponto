# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'
#Rails.application.config.assets.precompile += %w( map.js )
Rails.application.config.assets.precompile += %w( map_utils.js )
Rails.application.config.assets.precompile += %w( CONTROLLER_maps_routes.js )
Rails.application.config.assets.precompile += %w( CONTROLLER_maps_publish.js )
Rails.application.config.assets.precompile += %w( CONTROLLER_maps_near.js )
Rails.application.config.assets.precompile += %w( all.css.scss )
Rails.application.config.assets.precompile += %w( photo_slide.css.scss )

Rails.application.config.assets.precompile += %w( utils.js )
Rails.application.config.assets.precompile += %w( all.js )

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
