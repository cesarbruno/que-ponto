class ChatRoomsChannel < ApplicationCable::Channel
	def subscribed
		#stream_from "chat_rooms_#{params['chat_room_id']}_channel"
		stream_from "chat_rooms"
	end

	def unsubscribed
	    # Any cleanup needed when channel is unsubscribed
	end

	def send_message(data)
	    # process data sent from the page
	    Message.create!(conteudo: data['message'], ChatRoom_id: data['chat_room_id'], usuario_id: current_usuario.id)
	end
end