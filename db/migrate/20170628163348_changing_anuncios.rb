class ChangingAnuncios < ActiveRecord::Migration[5.0]
  def change
  	change_table :anuncios do |t|
  		#  Estruturada para negocio?
  		t.boolean :estruturado_negocio
  		t.string :estruturado_negocio_descricao
  		#  FIM Estruturado para negocio?

  		#  Imovel viavel 
  		t.string :imovel_viavel
  		#  FIM imovel viavel

  		#  Corretores
  		t.boolean :corretores
  		t.decimal :comissao_aluguel
  		t.decimal :comissao_venda
  		#  FIM Corretores

  		#  Contato
  		t.boolean :contato_apenas_corretor
  		#  FIM  Contato

  		#  Salvar sem publicar
  		t.boolean :publicar
  		#  FIM Salvar sem publicar

  		#  Diferenciais
  		t.boolean :mezanino
  		t.boolean :armazem
  		t.boolean :esquina
  		t.boolean :area_externa
  		t.string :cadeirante #Sim, Nao, Parcial
  		t.string :cadeirante_descricao
  		# FIM Diferenciais

  		# Estacionamento
  		t.boolean :estacionamento_compartilhado
  		# FIM Estacionamento

  	end
  end
end
