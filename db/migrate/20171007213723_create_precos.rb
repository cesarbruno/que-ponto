class CreatePrecos < ActiveRecord::Migration[5.0]
  def change
    create_table :precos do |t|
      t.float :preco_aluguel
      t.float :preco_venda
      t.float :preco_ponto
      t.float :preco_condominio
      t.float :preco_iptu
      t.boolean :preco_negociavel
      t.float :preco_outros
      t.string :preco_outros_descricao
      t.belongs_to :anuncio, foreign_key: true

      t.timestamps
    end
  end
end
