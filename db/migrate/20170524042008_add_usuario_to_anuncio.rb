class AddUsuarioToAnuncio < ActiveRecord::Migration[5.0]
  def change
    add_reference :anuncios, :usuario, foreign_key: true
  end
end
