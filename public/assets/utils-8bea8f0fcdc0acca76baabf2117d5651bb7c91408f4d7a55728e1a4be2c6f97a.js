var KMLLayer = function(){

	this.mapa;
	this.nome_bairros = [];
	this.codigos = [];

	this.poligonos = [];

	this.setMapa = function(xml){
		/**********************************************************************
		 *		Este método desenha modela e salva os polígonos a partir de   *
		 *	um documento KML já existente.									  *
		 **********************************************************************/

	    var coordinatesTwo = []; // Array auxiliar, guarda o retorno da leitura do KML
	    var pathPolygonArray = []; // Array que guardar os pares ordenados [LAT, LNG]
	    var codigoSencitario = []; // Array que guarda todos os códigos sencitários em ordem
	    var path = []; // Array que é posta dentro do maps.Polygon para que o polígono possa ser desenhado.
	    var POLYGON = []; // Array que guarda os polígonos plotados no mapa
	    var nomeBairros = []; //Nome dos bairros
	    var xmlDoc = xml.responseXML; // Pega as tags do KML
	    var codigo = xmlDoc.getElementsByTagName("name"); // Pega todos os códigos secitários dentro do KML
	    var coordinates = xmlDoc.getElementsByTagName("coordinates"); // Pega todas as coordenadas de cada código sencitário
	    var bairros = xmlDoc.getElementsByTagName("SimpleData"); // Pega os bairros no arquivo KML


	    var counterBairros = 5;
	    for (var i = 0; i < coordinates.length; i++) {
	    	/*						Bairros
	    	 *
		     *	Este loop serve para converter a captura, que é um DOM, em uma array de strings
	    	 */
	        if (bairros[counterBairros] != undefined) {
	            var bairro = bairros[counterBairros].childNodes[0].nodeValue;
	            nomeBairros.push(bairro);
	            counterBairros = counterBairros + 6; 
	        }
	        

	        var result = coordinates[i].childNodes[0].nodeValue;
	        coordinatesTwo.push(result);
	    }

	    for (var i = 2; i < codigo.length; i++) {
	    	/*						Código Cencitário
	    	 *
		     *	Este loop serve para converter a captura, que é um DOM, em uma array de string
		     *	Começa com i = 2 pois há outras duas tags <name> acima do início de onde desejamos
	    	 */
	        var cd = codigo[i];
	        var cod = cd.childNodes[0].nodeValue; 
	        codigoSencitario.push(cod);
	    }

	    /**********************************************************************************
	    		Até aqui já temos:
	    			Códigos Sencitários salvos em uma variável array => codigoSencitario
	    			Coordenadas de cada Código Sencitário salvos em uma variável array => coordinatesTwo
	    			Nome dos bairros salvos em uma variável array => 
	    **********************************************************************************/

	    for (var i = 0; i < coordinatesTwo.length; i++){
	    	/*					Coordenadas
	    	 *
		     *	Este loop serve para organizar as coordendas.
		     *	Primeiro separada os pontos e depois organiza Lat e Lng
	    	 */
	    	var x = coordinatesTwo[i].split(" "); // Separa as coordenadas de pontos distintos
	    	x = x.toString(); //Converte tudo que, agora esta junto, numa string (todas coordenadas)
	    	var y = x.split(","); // Agora separa todos itens da string, os tornando idependentes. "Transforma numa array".
	    	/*
				Para a próxima linha de código:
	    			A ordem no doc KML é a seguinte: 
	    				LONGITUDE, LATITUDE, ALTITUDE
	    	*/	    	
	    	var indexLat = 1; //Posição latitude
	    	var indexLng = 0; //Posição longitude
	        pathPolygonArray[i] = []; //Cria uma array que irá receber os pares, agora ordenados, de lat e lng

	    	for (var j = 0; j < y.length; j++) {
	    		/*
			     *	Este loop serve para organizar os pares ordenados, os deixando agora da seguinte forma:
				 *					     pathPolygonArray[i]
				 *			                [
				 *			                    [lat, lng],
				 *			                    [lat, lng],
				 *			                    ...
				 *			                ]
		    	 */
	            pathPolygonArray[i].push([y[indexLat], y[indexLng]]);
	    		indexLat = indexLat + 3; //Ajusta posição latitude
	    		indexLng = indexLng + 3; //Ajusta posição longitude
	    	}    
	    }

	    /**********************************************************************************
	    		Até aqui já temos:
	    			Códigos Sencitários salvos em uma variável array => codigoSencitario
	    			Coordenadas de cada Código Sencitário salvos em uma variável array => coordinatesTwo

	    		Agora na mesma ordem do código sencitário está organizando uma array 2d com as respectivas coordenadas
	    		de cada código sencitário
		    			i = index do código sencitário
		    			n = todas as coordenadas
		    			pathPolygonArray[i][n]
	    **********************************************************************************/

	    for (var i = 0; i < codigoSencitario.length; i++) {
	    	/*
		     *	Este loop percorre pathPolygonArray para que possa ser criado o object LatLng assim como requerido pelo Google
	    	 */
	        var polygonPath = pathPolygonArray[i];

	        var x = [];

	        for (var j = 0; j < polygonPath.length; j++) {
	        	/*
			     *	Este loop verifica e cria o objeto LatLng requerido pelo Google.
		    	 */
	            if (polygonPath[j] != "," && ! ( polygonPath[j][0] == undefined || polygonPath[j][0] == undefined )) {
	                var y = new google.maps.LatLng(parseFloat(polygonPath[j][0]),parseFloat(polygonPath[j][1]));
	                x.push(y);
	            }
	        }
	        path.push(x); // Path recebe os objetos criados para depois poder usá-los na hora de plotar o polígono
	    }

	    /**********************************************************************************
	    		Agora é a parte final, depois de todos os dados organizados em:
	    			codigoSencitario => guardando todos os códigos sencitários, um para cada polígono
	    			path => guardando todas as coordenadas de cada polígono referente a um código sencitário
	    **********************************************************************************/
	    var addListenersOnPolygon = function(polygon, index) {
	    	/*
			 *	Esta função é apenas um event handler para criar o "onClick"
		     */
		  google.maps.event.addListener(polygon, 'click', function (event) {
		  	alert("Clicado");
	        //getPlacesByClick(this.mapa, event.latLng, codigoSencitario, nomeBairros, POLYGON);
		  });  
		}

	    for (var i = 0; i < codigoSencitario.length; i++) {
	    	/*
			 *	Este loop cria todos os polígonos e seta todas os listeners
		     */

	        // Construct the polygon.
	        var poligono = new google.maps.Polygon({
	        	paths: path[i],
	            map: this.mapa,
	            strokeColor: '#FF0000',
	            strokeOpacity: 0.1,
	            strokeWeight: 2,
	            fillColor: '#FF0000',
	            fillOpacity: 0.05
	        });
	        addListenersOnPolygon(poligono, i);
	        POLYGON.push(poligono);
	    }








	    //ClickNosTipos(this.mapa, codigoSencitario, nomeBairros, POLYGON);
	    //FirstOpen(this.mapa, pos);
	}

}

var poligono = function(){

	this.codigo; //Código Sencitário
	this.bairro; //Nome do Bairro
	this.vertices; //Vertcices que formam o polígono
	this.poligono; //Objeto Google Polygon


}

var Mapa = function(){

	this.mapa;
	this.caixaBusca;
	this.marcadores = [];
	this.currentLocal;
	this.camadaTransito;
	this.streetView;

	this.tiposLugares = ["bank"];
	this.circulos = [];
	this.raio = 500;
	this.centro;



	this.setKml = function(){
		var xhttp;
		var getXHR = function(){
		        try{return new XMLHttpRequest();}   catch(e){}
		        try{return new ActiveXObject("Msxml2.XMLHTTP.6.0");}    catch(e){}
		        try{return new ActiveXObject("Msxml2.XMLHTTP.3.0");}    catch(e){}
		        try{return new ActiveXObject("Microsoft.XMLHttp");}     catch(e){}
		        console.log("Could not find XMLHttpRequest");
		    };
		    xhttp = getXHR();
		if (xhttp) {
		    //var url = "<%= File.join(Rails.root, 'assets', 'kml', 'Recife.kml') %>"; // Pega a URL do rails
			//var url = "http://usepio.com/Recife.kmz";
		    var url = "http://www.ptransparencia.com.br/arquivos/camarasurubim/Recife.kml";
		    xhttp.open("GET", url, true);
		    var mapa = this.mapa;
			xhttp.onreadystatechange = function() {
			    if (this.readyState == 4 && this.status == 200) {
			    	var kml = new KMLLayer;
			    	kml.mapa = mapa;
			    	kml.setMapa(this);
			        //readXML(this);
			    }
			};
		    xhttp.onerror= function(e) {
		        alert("Error fetching " + url);
		    };
		    xhttp.send();			
		}
	}

	this.getTooltips = function(url){
		var xhttp, response;
		var getXHR = function(){
		        try{return new XMLHttpRequest();}   catch(e){}
		        try{return new ActiveXObject("Msxml2.XMLHTTP.6.0");}    catch(e){}
		        try{return new ActiveXObject("Msxml2.XMLHTTP.3.0");}    catch(e){}
		        try{return new ActiveXObject("Microsoft.XMLHttp");}     catch(e){}
		        console.log("Could not find XMLHttpRequest");
		    };
		    xhttp = getXHR();
		if (xhttp) {
		    xhttp.open("GET", url, true);
		    var mapa = this.mapa;
		    var centro = this.centro;
			xhttp.onreadystatechange = function() {
			    if (this.readyState == 4 && this.status == 200) {
			    	var result = JSON.parse(this.response);
			    	for (var i = result.length - 1; i >= 0; i--) {
			    		

			    		obj = result[i];
			    		//console.log(obj);
			    		if (obj == undefined) continue;
			    		var contentString =
				           "<section>"+
				                "<header>"+
				                    "<h1>"+
				                        obj.nome
				                    +"</h1>"
				                +"</header>"
				                +"<article>"+
				                    "<p>"+
				                        obj.descricao
				                    +"</p>"
				                +"</article>"+
				            +"</section>";

				        

				        
				        var request = {
				            location: centro,
				            radius: '5000',
				            query:  obj.logradouro + ',' + obj.numero + '-' + obj.bairro + '-' + obj.cidade + '-' + obj.estado
				          };

				          var service = new google.maps.places.PlacesService(mapa);
				          service.textSearch(request, callback);

				        function callback(results, status) {
				          if (status == google.maps.places.PlacesServiceStatus.OK) {
				            for (var i = 0; i < results.length; i++) {
				              if (results[i] == undefined) continue;
				              setMarcadoresSimple(mapa, results[i], null, contentString);
				              break;
				            }
				          }
				        }
			    	}			    	
			    }
			};
		    xhttp.onerror= function(e) {
		        alert("Error fetching " + url);
		    };
		    xhttp.send();	
		    
		}
	}


	this.setMapa = function(div){
		var mapOP = { zoom: 16 };
		this.mapa = new google.maps.Map(div, mapOP);
	}

	var getMapa = function(){
		return this.mapa;
	}

	this.setCentro = function(centro){
		this.centro = centro;
		this.mapa.setCenter(centro);
	}
	var getCentro = function(){
		return this.centro;
	}

	var setRaio = function(raio){
		this.raio = raio;
	}
	var getRaio = function(){
		alert(this.raio);
	}


	this.setCirculo = function(centro = null, locais = null){
		if (this.circulos.length != 0) {
			deleteCirculos();
			deleteMarcadores();
		}
		var mapa = this.mapa;
	    var tiposLugares = this.tiposLugares;
	    var marcadores = this.marcadores;
	    var raio = this.raio;
		getBuscarLocal(mapa, centro, raio, tiposLugares, marcadores, locais);
		var circle = new google.maps.Circle({
	      	strokeColor: '#FF0000',
	      	strokeOpacity: 0.8,
	      	strokeWeight: 2,
	      	fillColor: '#FF0000',
	      	fillOpacity: 0.35,
	      	map: this.mapa,
	      	center: (centro == null ? getCentro() : centro),
	      	radius: this.raio,
	      	editable: true
	    });

	    
	    google.maps.event.addListener(circle, 'radius_changed', function(event){
	    	var raio = this.getRadius();
	    	setRaio(raio);
	    	marcadores = deleteMarcadores(marcadores);
	    	getBuscarLocal(mapa, centro, raio, tiposLugares, marcadores, locais);

	    });
    	this.circulos.push(circle);
	}

	var getBuscarLocal = function(mapa, centro, raio, tiposLugares, marcadores, locais = null){
        var service = new google.maps.places.PlacesService(mapa);
        //console.log(tiposLugares);
        service.nearbySearch({
          	location: centro,
          	radius: raio,
          	type: tiposLugares
        }, callback);
        function callback(results, status) {
	        if (status === google.maps.places.PlacesServiceStatus.OK) {	
	        	
	          	for (var i = 0; i < results.length; i++) {
	            	setMarcadores(mapa, results[i], marcadores, tiposLugares);
	          	}
	          	if (locais != null) {
	          		for (var i = 0; i < locais.length; i++) {
	          			setMarcadores(mapa, locais[i], marcadores, tiposLugares);
	          		}
	          	}	          	
	        }
	    }
	}

	this.deleteMarcadores = function(){
		for (var i = this.marcadores.length - 1; i >= 0; i--) {
			this.marcadores[i].setMap(null);
		}
        this.marcadores = [];
	}

	var deleteMarcadores = function(array = null){
		if (array != null) {
			for (var i = array.length - 1; i >= 0; i--) {
				array[i].setMap(null);
			}
	        array = [];
	        return array;
		}
		for (var i = this.marcadores.length - 1; i >= 0; i--) {
			this.marcadores[i].setMap(null);
		}
        this.marcadores = [];
	}
	var deleteCirculos = function(array = null){
		if (array == null) {
			for (var i = 0; i < this.circulos.length; i++) {
				this.circulos[i].setMap(null);
			}
			this.circulos = [];
		}else{
			for (var i = 0; i < array.length; i++) {
				array[i].setMap(null);
			}
			array = [];
			return array;
		}
	}

	this.setMarcadores = function(place = null, pos = null, div = null){
		var marker;
		var icone = getIcone('anuncio');
		if (place != null && pos == null) {
			var pos = place.geometry.location;
			var types = place.types;
			types = types.toString();
			marker = new google.maps.Marker({
				map: this.mapa,
				position: pos,
				title: types,
				icon: icone
			});
			this.marcadores.push(marker);
		}else if (place == null && pos != null){
			marker = new google.maps.Marker({
				map: this.mapa,
				position: pos,
				title: null,
				icon: icone
			});
			this.marcadores.push(marker);
		}		
		if (div != null) {
			var infowindow = new google.maps.InfoWindow({
	          	content: div
	        });
        	marker.addListener('click', function() {
          		infowindow.open(this.mapa, marker);
        	});
		}

		if (this.marcadores.length > 1) {
			var limits = new google.maps.LatLngBounds();
			for (var i = 0; i < this.marcadores.length; i++) {
				limits.extend(this.marcadores[i].position);
			}
			this.mapa.fitBounds(limits);
		}else{
			this.setCentro(this.marcadores[0].position);
		}		
	}

	var setMarcadoresSimple = function(mapa, place = null, pos = null, div = null){
		var marker;
		var icone = getIcone('anuncio');
		if (place != null && pos == null) {
			var pos = place.geometry.location;
			var types = place.types;
			types = types.toString();
			marker = new google.maps.Marker({
				map: mapa,
				position: pos,
				title: types,
				icon: icone
			});
			mapa.setCenter(pos);
			mapa.setZoom(13);
			//this.marcadores.push(marker);
		}else if (place == null && pos != null){
			marker = new google.maps.Marker({
				map: mapa,
				position: pos,
				title: null,
				icon: icone
			});
			mapa.setCenter(pos);
			mapa.setZoom(13);
			//this.marcadores.push(marker);
		}		
		if (div != null) {
			var infowindow = new google.maps.InfoWindow({
	          	content: div
	        });
        	marker.addListener('click', function() {
          		infowindow.open(mapa, marker);
        	});
		}

		/*if (this.marcadores.length > 1) {
			var limits = new google.maps.LatLngBounds();
			for (var i = 0; i < this.marcadores.length; i++) {
				limits.extend(this.marcadores[i].position);
			}
			this.mapa.fitBounds(limits);
		}else{
			this.setCentro(this.marcadores[0].position);
		}*/		
	}

	var setMarcadores = function(mapa, place, marcadores, tipos){
		var pos = place.geometry.location;
		var types = place.types;
		var id = place.placeId;
		var salvar = false;

		for (var i = 0; i < tipos.length; i++) {
			for (var j = 0; j < types.length; j++) {
				if (tipos[i] == types[j]) {
					salvar = true;
					break;
				}
			}
			if (salvar) {
				for (var i = 0; i < marcadores.length; i++) {
					//console.log(marcadores[i].getLabel());
					if(marcadores[i].getLabel()){
						if (marcadores[i].getLabel() != id) {
							types = types.toString();
							var marker = new google.maps.Marker({
								map: mapa,
								position: pos,
								title: types,
								label: id
							});	
							console.log(marker.getTitle());
							marcadores.push(marker);
							break;
						}					
					}else{
						types = types.toString();
							var marker = new google.maps.Marker({
								map: mapa,
								position: pos,
								title: types,
								label: id
							});	
							console.log(marker.getTitle());
							marcadores.push(marker);
							break;
					}
				}
				break;
			}
		}

		/*if (marcadores.length > 1) {
			var limits = new google.maps.LatLngBounds();
			for (var i = 0; i < marcadores.length; i++) {
				limits.extend(marcadores[i].position);
			}
			mapa.fitBounds(limits);
		}else{
			mapa.setCenter(marcadores[0].position);
		}*/
	}

	var getIcone = function(type){
		var imagem;
		if (type == 'anuncio') {
			imagem = "/assets/maps_icons/bus_station-40aad730befed1dc527551bdf460f9ef377316e26f76ebc8c76c637817c29210.png";
		}else if(type == 'bus_station'){
			imagem = "/assets/maps_icons/bus_station-40aad730befed1dc527551bdf460f9ef377316e26f76ebc8c76c637817c29210.png";
		}else if(type == 'taxi_stand'){
			imagem = "/assets/maps_icons/taxi_stand-155d4568892a2db920bf8a8032e7a944d29c5a784ba7a9cd2211c077cb049620.png";
		}else if(type == 'subway_station'){
			imagem = "/assets/maps_icons/subway_station-dd3f78f2110b5a52169e169556354ec0b1edbe9d39c52b3e1b1a7bb759995f23.png";
		}else{
			return null;
		}
		var iconResized = {
			url: imagem, // url
			scaledSize: new google.maps.Size(50, 50)//, // scaled size
			//origin: new google.maps.Point(0,0), // origin
			//anchor: new google.maps.Point(0, 0) // anchor
		};
		return iconResized;
	}

	this.setCaixaBusca = function(div, insideMap = false){
		/*
			Esse método serve para ativar a BUSCA DO GOOGLE no mapa;

				div => HTMLElement

			Mostra e libera o uso da caixa de busca
		*/
		if (insideMap) {
			div.style.display = "block";
			this.mapa.controls[google.maps.ControlPosition.TOP_LEFT].push(div);
		}		
		this.caixaBusca = new google.maps.places.SearchBox(div);
		
		/*		Gamby para funcionar com os assícronos 		*/
		var box = this.caixaBusca;
		var mapa = this.mapa;
		var marcadores = this.marcadores;
		/*		FIM		Gamby para funcionar com os assícronos 		*/

		// Avisa e libera o resulta da caixaBusca no mapa
	    this.mapa.addListener('bounds_changed', function() {
	    	box.setBounds(mapa.getBounds());
	    });
		// Quando o local é alterado no caixaBusca executa essa função
	    this.caixaBusca.addListener('places_changed', function() {
		    var places = box.getPlaces();

		    if (places.length == 0) {
		      return;
		    }

		    marcadores = deleteMarcadores(marcadores);

		    // Pega o nome e a localização de cada lugar.
		    var bounds = new google.maps.LatLngBounds();

		    places.forEach(function(place) {
		    	if (!place.geometry) {
	              	console.log("Algo deu errado");
	              	return;
	            }
			    var icon = {
			    	url: place.icon,
			        size: new google.maps.Size(71, 71),
			        origin: new google.maps.Point(0, 0),
			        anchor: new google.maps.Point(17, 34),
			        scaledSize: new google.maps.Size(25, 25)
			    };

		      	// Cria uma marca para cada lugar.
		      	marcadores.push(new google.maps.Marker({
		        	map: mapa,
		        	icon: icon,
		        	title: place.name,
		        	position: place.geometry.location
		      	}));
		      	this.marcadores = marcadores;


		      	if (place.geometry.viewport) {
		        	// Only geocodes have viewport.
		        	bounds.union(place.geometry.viewport);
		      	} else {
		        	bounds.extend(place.geometry.location);
		      	}
		    });
		    mapa.fitBounds(bounds);		    
	    });
	}

	this.getLocal =  function(){
		var pos;
		var mapa = this.mapa;
		if (navigator.geolocation) {
			
	        navigator.geolocation.getCurrentPosition(function (position) {
	            pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	            mapa.setCenter(pos);
	        });
	        

	    }else{
	    	pos = new google.maps.LatLng(-8.0538900, -34.8811100);
	    	mapa.setCenter(pos);
	    }	    
	}

	this.setTransito = function(){
		this.camadaTransito = new google.maps.TrafficLayer();
  		this.camadaTransito.setMap(this.mapa);
	}

	this.setStreetView = function(div, pos = null){
		var mapa = this.mapa;
		google.maps.event.addListenerOnce(mapa, 'idle', function(){
			//var mapa = this;
			var atual = mapa.getCenter();
			
			this.streetView = new google.maps.StreetViewPanorama(
		    div, {
		    	position: (pos == null ? atual : pos),
		        pov: {
		        	heading: 34,
		        	pitch: 10
		        }
		    });
		  	mapa.setStreetView(this.streetView);

		});		
	}

	this.clickFiltroTipos = function(div, tipo){
		var op = div;

		//op.onclick = function(){
			for (var i = 0; i < this.tiposLugares.length; i++) {

				//Deletar
				if(this.tiposLugares[i] == tipo){
					if (this.tiposLugares.length == 1) {
						alert("Você não pode deixar vazio");
						op.checked = true;
						break;
					}
					var test = true;
					var toDelete = [];
					for (var i = 0; i < this.marcadores.length; i++) {
						if (this.marcadores[i].getTitle() != null) {
							var tipos = this.marcadores[i].getTitle().split(",");
							for (var j = 0; j < tipos.length; j++) {
								if (tipos[j] == tipo) {
									this.marcadores[i].setMap(null);
									toDelete.push(i);
									if (test) {
										test = false;
										var index = this.tiposLugares.indexOf(tipo);	
										this.tiposLugares.splice(index, 1);
									}									
								}							
							}
						}						
					}
					if (toDelete.length > 0) {
						for (var i = 0; i < toDelete.length; i++) {
							this.marcadores.splice(toDelete[i], 1);							
						}
					}
					if (test) {
						var index = this.tiposLugares.indexOf(tipo);
						this.tiposLugares.splice(index, 1);
					}
					break;
				}else if(i == this.tiposLugares.length - 1){
					this.tiposLugares.push(tipo);
					var mapa = this.mapa;
					var centro = (this.centro == undefined ? new google.maps.LatLng(-8.0738900, -34.9011100) : this.centro);
					var raio = this.raio;
					var tiposLugares = this.tiposLugares;
					var marcadores = this.marcadores;

					getBuscarLocal(mapa, centro, raio, tiposLugares, marcadores);
					break;
				}
			}
		//}
	}

}
;
