/********************************************************
*
*					UTILS
*
*********************************************************/

var Utils = function(){

};

Utils.prototype.OpenConncetion = function(){
    try{return new XMLHttpRequest();}   catch(e){}
    try{return new ActiveXObject("Msxml2.XMLHTTP.6.0");}    catch(e){}
    try{return new ActiveXObject("Msxml2.XMLHTTP.3.0");}    catch(e){}
    try{return new ActiveXObject("Microsoft.XMLHttp");}     catch(e){}
    console.log("Could not find XMLHttpRequest");
};

Utils.prototype.CalculateDistanceBetween2PointsLatLng = function(from, to){
	function deg2rad(deg) {
	  return deg * (Math.PI/180)
	}
	lat1 = from.lat();
	lon1 = from.lng();
	lat2 = to.lat();
	lon2 = to.lng();
  	var R = 6371; // Radius of the earth in km
  	var dLat = deg2rad(lat2-lat1);
  	var dLon = deg2rad(lon2-lon1); 
  	var a = 
	    Math.sin(dLat/2) * Math.sin(dLat/2) +
	    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
	    Math.sin(dLon/2) * Math.sin(dLon/2)
	    ; 
  	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  	var d = R * c; // Distance in km
  	return d * 1000; // Distance in meters
};

Utils.prototype.CleanArray = function(new_array, base_array){
	if (typeof new_array == 'string') new_array = new_array.split(',');
	if (typeof base_array == 'string') base_array = base_array.split(',');
	for (var i = 0; i < base_array.length; i++) {
		if(new_array.indexOf(parseInt(base_array[i])) >= 0){//includes(base_array[i])){
			base_array.splice(i, 1);
			i--;
		}
	}
	return base_array;
};

/********************************************************
*
*					end UTILS
*
*********************************************************/

/********************************************************
*
*					MAPA
*
*********************************************************/
// V => Has no return

var Map = function(div, zoom = null, centro = null, mapOp = null){
	// 	Construct
	if (mapOp == null) {
		mapOp = { zoom: (zoom == null ? 16 : zoom) }; // Zoom opções
	}	
	this.map = new google.maps.Map(div, mapOp); // Criação do mapa
	if (centro != null) {
		this.center = centro;
		this.V_SetCenter(centro);
	}else{
		this.center = centro; // centro do Mapa
	}	
	this.markers = []; // Array de marcadores do mapa {m: marker, id: id, types: types(array)}
	this.circle = undefined; // var to keep only one circle in the map
	this.types = ["bank"]; // Array that keeps the types of places to search
	this.trafficLayer = null; // Traffic layer to be set on map
	this.codes = []; // Array that save the coddigo sencitario
};


/*	DELETEs methods */
	Map.prototype.V_DeleteMerkers = function(index = null){
		// Method to clean all markers
		if (index == null) {
			for (var i = this.markers.length - 1; i >= 0; i--) {
				this.markers[i].m.setMap(null);
			}
		    this.markers = [];
		}else{
			this.markers[index].m.setMap(null);
			this.markers.splice(index, 1);
		}
	};

	Map.prototype.V_DeleteCircle = function(){
		// Method to clean off the circle
		this.circle.setMap(null);
		this.circle = undefined;
	};
/*	END DELETEs methods */


/*	UPDATEs methods */
	Map.prototype.V_UpdateMarker = function(logradouro, numero, bairro, cidade){
		// Update the marker in map (new anuncio)
		this.V_DeleteMerkers();
		var request = {
			location: this.GetCenter(),
			radius: '5000',
			query:  logradouro + ',' + numero + bairro + '-' + cidade
		};

		var service = new google.maps.places.PlacesService(this.map);
		service.textSearch(request, callback);

		var self = this;
		function callback(results, status) {
			if (status == google.maps.places.PlacesServiceStatus.OK) {
				if(results.length > 0){
					if (results[0] != undefined){
					    var marker = self.SetMarker(results[0].geometry.location, true, "Arraste", null, null);
					    self.V_SetCenter(results[0].geometry.location);
					    marker.addListener('dragend', function(){
		                    Mapa.V_GetAddress(marker.getPosition().lat(), marker.getPosition().lng());
		                });
					}else{
						alert("Endereço não encontrado!");
						console.log("Erro no método V_UpdateMarker: primeiro resultado é undefined.");
					}
				}
			}
		}
	};

	Map.prototype.V_UpdatePlaces = function(del = false, radius_change = false){
		// Method to update the places based on types attribute
		if (del) {
			// To delete
				for (var i = 0; i < this.markers.length; i++) {
					// For each marker
					if (this.markers[i].types != null) {
						var del = true;
						for (var n = 0; n < this.markers[i].types.length; n++){
							// for each marker's type
							for (var k = 0; k < this.types.length; k++) {
							// For each type
								if (this.types[k] == this.markers[i].types[n]) {
									del = false;
									break;
								}
							}
						}
						if (del) {
							this.markers[i].m.setMap(null);
							this.markers.splice(i, 1);
							i--;
						}
					}

				}

		}else{
			//To add
			if (radius_change) this.V_DeleteMerkers();
			var service = new google.maps.places.PlacesService(this.map);
		    service.nearbySearch({
		      	location: this.circle.getCenter(),
		       	radius: this.circle.getRadius(),
		       	types: this.types
		    }, callback);
		    var self = this;
		    function callback(results, status){
		       	if (status === google.maps.places.PlacesServiceStatus.OK){
		       		//console.log(results.length);
		       		for (var i = 0; i < results.length; i++) {
		       			var id = results[i].place_id;
		       			var types = results[i].types;
		       			var name = results[i].name;
		       			var description = results[i].html_attributions;
		       			var icon = results[i].icon;
		       			var address = results[i].vicinity;
		       			var text = "" + description + "<br>" + address;
		       			var tooltip = self.SetTooltipDiv(name, text);
				       	// Clean the types that's not important
				       	for (index in types){
	        				if (types[index] == "point_of_interest") {
					    		types.splice(index, 1);
					    	}
					    	if (types[index] == "establishment") {
					    		types.splice(index, 1);
					    	}
	        			}
				       	if (results[i] != undefined) {
				       		self.SetMarker(results[i].geometry.location, false, name, icon, id, types, tooltip);
				       		//break;
				       	}
		       			/*if (self.markers.length == 0) {
		       				var types = results[i].types;
		       				var id = results[i].place_id;
		       				var name = results[i].name;
		       				var description = results[i].html_attributions;
		       				var icon = results[i].icon;
		       				var address = results[i].vicinity;
		       				var text = "" + description + "<br>" + address;
		       				var tooltip = self.SetTooltipDiv(name, text);		       				
				       			// Clean the types that's not important
				       			for (var i = 0; i < types.length; i++) {
				       				if (types[i] == "point_of_interest") {
				       					types.splice(i, 1);
				       				}
				       				if (types[i] == "establishment") {
				       					types.splice(i, 1);
				       				}
				       			}
				       			console.log(tooltip);
				       			if (results[i] != undefined) {
				       				self.SetMarker(results[i].geometry.location, false, name, icon, id, types, tooltip);
				       				break;
				       			}
		       			}else{
		       				for (var index = 0; index < self.markers.length; index++) {
			       				if (self.markers[index].id == results[i].place_id) {
			       					console.log("Hello");
			       					break;
			       				}else if(index == this.markers.length - 1){
			       					var types = results[i].types;
				        			var id = results[i].place_id;
				        			var name = results[i].name;
				        			var description = results[i].html_attributions;
				        			var icon = results[i].icon;
				        			var address = results[i].vicinity;
				        			var text = "" + description + "<br>" + address;
				        			var tooltip = self.SetTooltipDiv(name, text);
				        			console.log(tooltip);
				        			
					       			// Clean the types that's not important
					       			for (var i = 0; i < types.length; i++) {
					       				if (types[i] == "point_of_interest") {
					       					types.splice(i, 1);
					       				}
					       				if (types[i] == "establishment") {
					       					types.splice(i, 1);
					       				}
					       			}
					       			console.log(results[i]);
					       			if (results[i] != undefined) {
					       				self.SetMarker(results[i].geometry.location, false, name, icon, id, types, tooltip);
					       				break;
					       			}
			       				}		       				
			       			}
		       			}*/
		       					       			
		       		}
		       	}
		    }
		}
	};
/*	END UPDATEs methods */


/*	SETs methods */
	
	Map.prototype.SetTooltipDiv = function(name, description){
		// Mehtod that create a DIV for the Tooltip
		var div = "<div>"+
			"<h3>" + name + "</h3>" +
			"<p>" + description + "</p>" +
		"</div>";
		return div;

	};

	Map.prototype.V_SetCenter = function(Objlatlng){
		// Method to change the center of map
		this.map.setCenter(Objlatlng);
	};

	Map.prototype.V_SetCenterByUserLocation = function() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(callback);
		}else {
			console.log("Geolocation is not supported by this browser.");
			var pos = new google.maps.LatLng(-8.0538900, -34.8811100); // Returna a default center
			this.V_SetCenter(pos);
		}
		var self = this;
		function callback(position){
			//Calback do getCurrentPosition
			var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			self.V_SetCenter(pos);
		}
	};

	Map.prototype.SetMarker = function(latlng, drag = false, title = null, icon = null, id = null, types = null, tooltip = null){
		//Method to set a new marker
		var marker;
		if (icon != null) {
			var image = {
			    url: icon,
			    scaledSize: new google.maps.Size(45, 45),
			    origin: new google.maps.Point(0, 0),
			    anchor: new google.maps.Point(0, 45)
			  };
			marker = new google.maps.Marker({
		        position: latlng,
		        map: this.map,
		        draggable: drag,
		        title: (title == null ? "" : title),
		        icon: image
		    });
		}else{
			marker = new google.maps.Marker({
		        position: latlng,
		        map: this.map,
		        draggable: drag,
		        title: (title == null ? "" : title)
		    });
		}
		if (tooltip != null) {
			var infowindow = new google.maps.InfoWindow({
	          	content: tooltip
	        });
        	marker.addListener('click', function() {
          		infowindow.open(this.map, marker);
        	});
		}
		var m = {m: marker, id: id, types: types, tooltip: tooltip};
		this.markers.push(m);
        var limits = new google.maps.LatLngBounds();
        for (var i = 0; i < this.markers.length; i++) {
          	limits.extend(this.markers[i].m.getPosition());
        }
        this.map.fitBounds(limits);
		return marker;
	};

	Map.prototype.V_SetCircle = function(center = null, radius = null){
		if (this.circle != undefined) {
			this.V_DeleteCircle();
		}
		var circle = new google.maps.Circle({
	      	strokeColor: '#3e4095',
	      	strokeOpacity: 0.8,
	      	strokeWeight: 2,
	      	fillColor: '#3e4095',
	      	fillOpacity: 0.35,
	      	map: this.map,
	      	center: (center == null ? this.GetCenter() : center),
	      	radius: (radius == null ? 1000 : radius),
	      	editable: true
	    });

	    var self = this;
	    circle.addListener('radius_changed', function(event){
	    	//Refazer pesquisa
	    	self.V_UpdatePlaces(false, true);
	    });
	    circle.addListener('center_changed', function(event){
	    	//Refazer pesquisa
	    	self.V_UpdatePlaces(false, true);
	    });
	    this.circle = circle;
	};

	Map.prototype.V_SetStreetView = function(pos, div){
		// Method to set the streetview
		var mapa = this.map;
		google.maps.event.addListenerOnce(mapa, 'idle', function(){
			var streetView = new google.maps.StreetViewPanorama(div,
				{
			    	position: pos,
			        pov: {
			        	heading: 34,
			        	pitch: 10
			        }
		    	});
		  	mapa.setStreetView(streetView);
		});
	};

	Map.prototype.V_SetTrafficLayer = function(){
		// Method to set the traffic layer
		if (this.trafficLayer == null) {
			this.trafficLayer = new google.maps.TrafficLayer();
  			this.trafficLayer.setMap(this.map);
		}else{
			this.trafficLayer.setMap(null);
			this.trafficLayer = null;
		}		
	};

	Map.prototype.V_SetBusStop = function(checked){
		// Method to show the bus stop
		if (checked) {
			var self = this;
			google.maps.event.addListenerOnce(this.map, 'idle', function(){
				var service = new google.maps.places.PlacesService(self.map);
		        service.nearbySearch({
		          	location: self.map.getCenter(),
		          	radius: 800,
		          	types: ["bus_station"]
		        }, callback);
		        function callback(results, status){
		        	if (status === google.maps.places.PlacesServiceStatus.OK){
		        		for (var i = 0; i < results.length; i++) {
		        			var types = results[i].types;
			       			var id = results[i].place_id;
			       			var name = results[i].name;
			       			var description = results[i].html_attributions;
			       			var icon = results[i].icon;
			       			var address = results[i].vicinity;
			       			var text = "" + description + "<br>" + address;
			       			var tooltip = self.SetTooltipDiv(name, text);		       				
					       	// Clean the types that's not important
					       	for (index in types){
		        				if (types[index] == "point_of_interest") {
						    		types.splice(index, 1);
						    	}
						    	if (types[index] == "establishment") {
						    		types.splice(index, 1);
						    	}
		        			}
					       	if (results[i] != undefined) {
					       		self.SetMarker(results[i].geometry.location, false, name, icon, id, types, tooltip);
					       	}
		        		}
		        	}
		        }
			});
		}else{
			// Clean filter
			for (var i = this.markers.length - 1; i >= 0; i--) {
				if (this.markers[i].types != null) {
					this.V_DeleteMerkers(i);
				}
			}
		}
		
	};


	Map.prototype.SetPolygon = function(points, color = '#FF0000'){
        // Construct the polygon.
        var bermudaTriangle = new google.maps.Polygon({
          paths: points,
          strokeColor: '#FF0000',
          strokeOpacity: 0.8,
          strokeWeight: 0.35,
          fillColor: color,
          fillOpacity: 0.35
        });
        bermudaTriangle.setMap(this.map);
        return bermudaTriangle;
	};

	Map.prototype.SetCodes = function(codes, xml, center){
		/**********************************************************************
		 *		Este método desenha modela e salva os polígonos a partir de   *
		 *	um documento KML já existente.									  *
		 **********************************************************************/

	    var coordinatesTwo = []; // Array auxiliar, guarda o retorno da leitura do KML
	    var pathPolygonArray = []; // Array que guardar os pares ordenados [LAT, LNG]
	    var codigoSencitario = []; // Array que guarda todos os códigos sencitários em ordem
	    var path = []; // Array que é posta dentro do maps.Polygon para que o polígono possa ser desenhado.
	    var POLYGON = []; // Array que guarda os polígonos plotados no mapa
	    var nomeBairros = []; //Nome dos bairros
	    var xmlDoc = xml.responseXML; // Pega as tags do KML
	    var codigo = xmlDoc.getElementsByTagName("name"); // Pega todos os códigos secitários dentro do KML
	    var coordinates = xmlDoc.getElementsByTagName("coordinates"); // Pega todas as coordenadas de cada código sencitário
	    var bairros = xmlDoc.getElementsByTagName("SimpleData"); // Pega os bairros no arquivo KML


	    var counterBairros = 5;
	    for (var i = 0; i < coordinates.length; i++) {
	    	/*						Bairros
	    	 *
		     *	Este loop serve para converter a captura, que é um DOM, em uma array de strings
	    	 */
	        if (bairros[counterBairros] != undefined) {
	            var bairro = bairros[counterBairros].childNodes[0].nodeValue;
	            nomeBairros.push(bairro);
	            counterBairros = counterBairros + 6; 
	        }
	        

	        var result = coordinates[i].childNodes[0].nodeValue;
	        coordinatesTwo.push(result);
	    }

	    for (var i = 2; i < codigo.length; i++) {
	    	/*						Código Cencitário
	    	 *
		     *	Este loop serve para converter a captura, que é um DOM, em uma array de string
		     *	Começa com i = 2 pois há outras duas tags <name> acima do início de onde desejamos
	    	 */
	        var cd = codigo[i];
	        var cod = cd.childNodes[0].nodeValue; 
	        codigoSencitario.push(cod);
	    }

	    /**********************************************************************************
	    		Até aqui já temos:
	    			Códigos Sencitários salvos em uma variável array => codigoSencitario
	    			Coordenadas de cada Código Sencitário salvos em uma variável array => coordinatesTwo
	    			Nome dos bairros salvos em uma variável array => 
	    **********************************************************************************/

	    for (var i = 0; i < coordinatesTwo.length; i++){
	    	/*					Coordenadas
	    	 *
		     *	Este loop serve para organizar as coordendas.
		     *	Primeiro separada os pontos e depois organiza Lat e Lng
	    	 */
	    	var x = coordinatesTwo[i].split(" "); // Separa as coordenadas de pontos distintos
	    	x = x.toString(); //Converte tudo que, agora esta junto, numa string (todas coordenadas)
	    	var y = x.split(","); // Agora separa todos itens da string, os tornando idependentes. "Transforma numa array".
	    	/*
				Para a próxima linha de código:
	    			A ordem no doc KML é a seguinte: 
	    				LONGITUDE, LATITUDE, ALTITUDE
	    	*/	    	
	    	var indexLat = 1; //Posição latitude
	    	var indexLng = 0; //Posição longitude
	        pathPolygonArray[i] = []; //Cria uma array que irá receber os pares, agora ordenados, de lat e lng

	    	for (var j = 0; j < y.length; j++) {
	    		/*
			     *	Este loop serve para organizar os pares ordenados, os deixando agora da seguinte forma:
				 *					     pathPolygonArray[i]
				 *			                [
				 *			                    [lat, lng],
				 *			                    [lat, lng],
				 *			                    ...
				 *			                ]
		    	 */
	            pathPolygonArray[i].push([y[indexLat], y[indexLng]]);
	    		indexLat = indexLat + 3; //Ajusta posição latitude
	    		indexLng = indexLng + 3; //Ajusta posição longitude
	    	}    
	    }

	    /**********************************************************************************
	    		Até aqui já temos:
	    			Códigos Sencitários salvos em uma variável array => codigoSencitario
	    			Coordenadas de cada Código Sencitário salvos em uma variável array => coordinatesTwo

	    		Agora na mesma ordem do código sencitário está organizando uma array 2d com as respectivas coordenadas
	    		de cada código sencitário
		    			i = index do código sencitário
		    			n = todas as coordenadas
		    			pathPolygonArray[i][n]
	    **********************************************************************************/

	    for (var i = 0; i < codigoSencitario.length; i++) {
	    	/*
		     *	Este loop percorre pathPolygonArray para que possa ser criado o object LatLng assim como requerido pelo Google
	    	 */
	        var polygonPath = pathPolygonArray[i];

	        var x = [];

	        for (var j = 0; j < polygonPath.length; j++) {
	        	/*
			     *	Este loop verifica e cria o objeto LatLng requerido pelo Google.
		    	 */
	            if (polygonPath[j] != "," && ! ( polygonPath[j][0] == undefined || polygonPath[j][0] == undefined )) {
	                var y = new google.maps.LatLng(parseFloat(polygonPath[j][0]),parseFloat(polygonPath[j][1]));
	                x.push(y);
	            }
	        }
	        path.push(x); // Path recebe os objetos criados para depois poder usá-los na hora de plotar o polígono
	    }

	    /**********************************************************************************
	    		Agora é a parte final, depois de todos os dados organizados em:
	    			codigoSencitario => guardando todos os códigos sencitários, um para cada polígono
	    			path => guardando todas as coordenadas de cada polígono referente a um código sencitário
	    **********************************************************************************/
	    /*var addListenersOnPolygon = function(polygon, index) {
	    	/*
			 *	Esta função é apenas um event handler para criar o "onClick"
		     *
		  google.maps.event.addListener(polygon, 'click', function (event) {
		  	alert("Clicado");
	        //getPlacesByClick(this.mapa, event.latLng, codigoSencitario, nomeBairros, POLYGON);
		  });  
		}*/
		this.V_SetCircle(center, 2000);// Circle to do the avaliation
	    for (var i = 0; i < codigoSencitario.length; i++) {
	    	/*
			 *	Este loop cria todos os polígonos e seta todas os listeners
		     */
		    for (var k = 0; k < path[i].length; k++) {
		     	if (this.circle.getBounds().contains(path[i][k])) {
		     		this.codes.push(codigoSencitario[i]);
		     		// Construct the polygon.
			        var poligono = new google.maps.Polygon({
			        	paths: path[i],
			            map: this.map,
			            strokeColor: '#FF0000',
			            strokeOpacity: 0.1,
			            strokeWeight: 2,
			            fillColor: '#FF0000',
			            fillOpacity: 0.05
			        });
			        //addListenersOnPolygon(poligono, i);
			        POLYGON.push(poligono);
			        //console.log(path[i]);
			        break;
		     	}	     	
		    }     
	    }
	    var utils = new Utils();
	    var true_codes = utils.CleanArray(codes, this.codes)
	    this.GetIbgeByCod(true_codes);//this.codes); // Get the information from db
	};

/* END SETs methods*/


/*	GETs methods */
	Map.prototype.GetPointInCircle = function(latlng){
		var d = google.maps.geometry.spherical.computeDistanceBetween(latlng, this.circle.center);
		var inside = (d <= this.circle.radius);
		//console.log("Nova: \nDistance: " + d + " / Raio: " + this.circle.radius);
		return inside;
		/*var lat = latlng.lat();
		var lng = latlng.lng();
		return this.circle.getBounds().contains( new google.maps.LatLng( lat, lng ));*/
	};

	Map.prototype.V_GetAddress = function(lat, lng){
		var utils = new Utils();
		var con = utils.OpenConncetion();

		if (con) {

	        var numero;
	        var logradouro;
	        var bairro;
	        var cidade;
	        var estado;



	        con.open("GET", "http://maps.googleapis.com/maps/api/geocode/json?latlng="+ lat +","+ lng +"&sensor=true", true);
	        con.onreadystatechange = function() {
	            if (this.readyState == 4 && this.status == 200) {
	                var result = JSON.parse(this.response);
	                                address = result.results[0].address_components[0].long_name;
	                                var num_or_street = result.results[0].address_components[0].long_name
	                                if (!isNaN(num_or_street)) {
	                                    numero = num_or_street;
	                                    logradouro = result.results[0].address_components[1].long_name;
	                                    bairro = result.results[0].address_components[2].long_name;
	                                    cidade = result.results[0].address_components[3].long_name;
	                                    estado = result.results[0].address_components[5].long_name;



	                                }else{
	                                    var quebrar = num_or_street.split("-");
	                                    if (quebrar.length >= 2) {
	                                        //Tira a média do intervalo de número da rua retornado
	                                        num_media = parseInt(quebrar[0]) + parseInt(quebrar[1]);
	                                        numero = parseInt(num_media) / 2;

	                                        logradouro = result.results[0].address_components[1].long_name;
	                                        bairro = result.results[0].address_components[2].long_name;
	                                        cidade = result.results[0].address_components[3].long_name;
	                                        estado = result.results[0].address_components[5].long_name;

	                                    }else{
	                                        logradouro = num_or_street;
	                                        numero = 0;
	                                        bairro = result.results[0].address_components[1].long_name;
	                                        cidade = result.results[0].address_components[2].long_name;
	                                        estado = result.results[0].address_components[4].long_name;

	                                    }
	                                }
	                                //console.log(result.results[0].address_components);

	                                document.getElementById("anuncio_logradouro").value = logradouro;
	                                document.getElementById("anuncio_bairro").value = bairro;
	                                document.getElementById("anuncio_cidade").value = cidade;
	                                document.getElementById("anuncio_numero").value = numero;
	                                document.getElementById("anuncio_estado").value = estado;
	                            }
	                        };
	                        con.onerror= function(e) {
	                            alert("Error fetching " + url);
	                        };
	                        con.send();


	                    }
	};

	Map.prototype.GetLatLng = function(address){
		var utils = new Utils();
		var con = utils.OpenConncetion();
		var latlng;

		if (con) {
	        con.open("GET", "http://maps.google.com/maps/api/geocode/json?address=" + address, false);
	        con.onreadystatechange = function() {
	            if (this.readyState == 4 && this.status == 200) {
	                var result = JSON.parse(this.response);
	                if (result.results[0]) {
	                	latlng = result.results[0].geometry.location;
	                }	                
	            };
	        }
	        con.onerror= function(e) {
	            alert("Error fetching " + url);
	        };
	        con.send();
	    }
	    return (latlng == undefined ? null : latlng);
	};

	Map.prototype.GetCenter = function(){
		// Method that returns the center of the map
		return this.map.getCenter();
	};

	Map.prototype.V_GetPlaces = function(pos, radius){
		// Method to return the places according the types
		var self = this;
		var places = [];
		google.maps.event.addListenerOnce(this.map, 'idle', function(){
			var service = new google.maps.places.PlacesService(self.map);
	        service.nearbySearch({
	          	location: pos,
	          	radius: radius,
	          	types: self.types
	        }, callback);
	        function callback(results, status){
	        	if (status === google.maps.places.PlacesServiceStatus.OK){
	        		for (var i = 0; i < results.length; i++) {
	        			var types = results[i].types;
		       			var id = results[i].place_id;
		       			var name = results[i].name;
		       			var description = results[i].html_attributions;
		       			var icon = results[i].icon;
		       			var address = results[i].vicinity;
		       			var text = "" + description + "<br>" + address;
		       			var tooltip = self.SetTooltipDiv(name, text);		       				
				       	// Clean the types that's not important
				       	for (index in types){
	        				if (types[index] == "point_of_interest") {
					    		types.splice(index, 1);
					    	}
					    	if (types[index] == "establishment") {
					    		types.splice(index, 1);
					    	}
	        			}
				       	if (results[i] != undefined) {
				       		self.SetMarker(results[i].geometry.location, false, name, icon, id, types, tooltip);
				       	}
	        		}
	        	}
	        }
		});
	};

	Map.prototype.GetMinutesDistanceBetweenTwoPoints = function(from, to, mode = "driving"){
		// This method returns the distance in time (minutes) between two points
		var utils = new Utils();
		var con = utils.OpenConncetion();
		var url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + from.lat() + "," + from.lng() + "&destinations="  + to.lat() + "," + to.lng() + "&mode=" + mode +"&key=AIzaSyDNspH1OpAwYIWsvMOV4hXbpRFl4Pfw_P0";
		var time_minutes = 0;
		if (con) {
	        con.open("GET", url, false);
	        con.onreadystatechange = function() {
	            if (this.readyState == 4 && this.status == 200) {
	                var result = JSON.parse(this.response);
	                if (result.rows[0].elements[0].status == "OK") {
	                	time_minutes = parseInt(result.rows[0].elements[0].duration.value / 60);
	                }
	            };
	        }
	        con.send();
	    }
	    return time_minutes;
	};

	Map.prototype.GetPointTimeDistance = function(point, direction, minutes, mode = "driving"){
		// This methods returns four points to draw a polygon
		var spherical = google.maps.geometry.spherical;

		//To
		var to;
		var d_to_point = 2000;
		var to_time = 0;
		var best = {point: {lat: 0, lng: 0}, time: 0};

		do{
			to = spherical.computeOffset(point, d_to_point, direction); // Get the point (lat, lng)
			to_time = this.GetMinutesDistanceBetweenTwoPoints(point, to, mode); // Get time distance of two points

			if(to_time > minutes){
				d_to_point = d_to_point - 100;
				best = {point: {lat: to.lat(), lng: to.lng()}, time: to_time};
								
			}else{
				if(to_time == minutes){
					// This is equals to what I want
					best = {point: {lat: to.lat(), lng: to.lng()}, time: to_time};
					break;
				}else{
					if (to_time > best.time){
						best = {point: {lat: to.lat(), lng: to.lng()}, time: to_time};
					}
					d_to_point = d_to_point - 100;
				}
			}
		}while(d_to_point > 0);

		return best;
	};

	Map.prototype.GetDistance = function(point, minutes){
		// This mehtods returns the the points of the polygon
		// Calculate the ditance for 36 points
		point = new google.maps.LatLng(point.lat, point.lng);

		var points_polygon = [];
		for (var degree = 0; degree <= 350; degree = degree + 10) {
			var r = this.GetPointTimeDistance(point, degree, minutes);//Mode
			points_polygon.push(r.point);
		}
		/*for (var i = 0; i < points_polygon.length; i++) {
			if (i == 0) {
				points_polygon[i] = (points_polygon[points_polygon.length - 1] + points_polygon[i] + points_polygon[i + 1])/3	
			}else if(i == points_polygon.length - 1){
				points_polygon[i] = (points_polygon[i - 1] + points_polygon[i] + points_polygon[0])/3
			}else{
				points_polygon[i] = (points_polygon[i - 1] + points_polygon[i] + points_polygon[i + 1])/3
			}
		}*/
		var polygon;
		switch(minutes){
			case 5:
				polygon = this.SetPolygon(points_polygon, '#FF0000');
				break;
			case 10:
				polygon = this.SetPolygon(points_polygon, '#0000FF');
				break;
			case 15:
				polygon = this.SetPolygon(points_polygon, '#FFFF00');
				var bounds = new google.maps.LatLngBounds();
		      	polygon.getPath().forEach(function (element, index) { bounds.extend(element); });
		      	this.map.fitBounds(bounds);
				break;
			default:
				polygon = this.SetPolygon(points_polygon);
				break;
		}
		return polygon;		
	};

	Map.prototype.V_FitPolygons = function(center, reference, second){
		// Fit the real size of times
		var quadrant;
		var newPath = [];
		var utils = new Utils();
		var referenceLatLng = reference.getPath().b;
		var secondLatLng = second.getPath().b;
		//reference = reference.getPath().b;
		(referenceLatLng[0].lat() > 0 ? quadrant = 1 : quadrant = 5);
		(referenceLatLng[0].lng() > 0 ? quadrant += 5 : quadrant += 7);
		switch(quadrant){
			case (1 + 5):
				//Case first quadrant
				// lat > 0 && lng > 0
				break;
			case (1 + 7):
				//Case second quadrant
				break;
			case (5 + 5):
				//Case fourth quadrant
				break;
			case (5 + 7):
				//Case third quadrant
				for (var i = 0; i < referenceLatLng.length; i++) {
					var dReference = utils.CalculateDistanceBetween2PointsLatLng(center, referenceLatLng[i]);
					var dSecond = utils.CalculateDistanceBetween2PointsLatLng(center, secondLatLng[i]);
					if (dSecond > dReference) {
						newPath.push(new google.maps.LatLng(referenceLatLng[i].lat(), referenceLatLng[i].lng()));
					}else{
						newPath.push(new google.maps.LatLng(secondLatLng[i].lat(), secondLatLng[i].lng()));
					}					
				}
				second.setPaths(newPath);
				
				/*secondLatLng = second.getPath().b;
				var pathArray = [];
				for (var i = 0; i < reference.length; i++) {
					// 18 times loop
					var outLng;
					var outLat;
					if (i < 9){
						// right side LATITUDE
						outLat = (reference[i].lat() < secondLatLng[i].lat() ?  true :  false);
						if (i <= 4) {
							// Top side of LONGITUDE
							outLng = (reference[i].lng() < secondLatLng[i].lng() ?  true :  false);
						}else{
							// Bottom side of LONGITUDE
							outLng = (reference[i].lng() < secondLatLng[i].lng() ?  true :  false);
						}
					}else if(i == 9){
						outLat = false;
						outLng = (reference[i].lng() > secondLatLng[i].lng() ?  true :  false);
					}else if(i == 0){
						outLat = false;
						outLng = (reference[i].lng() < secondLatLng[i].lng() ?  true :  false);
					}else{
						// left side LATITUDE
						outLat = (reference[i].lat() > secondLatLng[i].lat() ?  true :  false);						
						if (i >= 14) {
							// Top side of LONGITUDE
							outLng = (reference[i].lng() > secondLatLng[i].lng() ?  true :  false);
						}else{
							// Bottom side of LONGITUDE
							outLng = (reference[i].lng() > secondLatLng[i].lng() ?  true :  false);
						}
					}
					var lat = (outLat ?  reference[i].lat() :  secondLatLng[i].lat());
					var lng = (outLng ?  reference[i].lng() :  secondLatLng[i].lng());
					console.log("Latitude Fora: " + outLat + "\nReferencia: " + reference[i].lat() + "\nComparação: " + secondLatLng[i].lat());
					pathArray.push(new google.maps.LatLng(lat, lng));
				}

				second.setPaths(pathArray);*/
				break;
			default:
				console.log("Erro");
				break;
		}
	};

	Map.prototype.GetCodes = function(codes, kml_file = "http://www.ptransparencia.com.br/arquivos/camarasurubim/Recife.kml"){
		var utils = new Utils();
		var xhttp = utils.OpenConncetion();
		if (xhttp) {
		    //var url = "<%= File.join(Rails.root, 'assets', 'kml', 'Recife.kml') %>"; // Pega a URL do rails
			//var url = "http://usepio.com/Recife.kmz";
		    var url = kml_file;
		    xhttp.open("GET", url, true);
		    var self = this;
			xhttp.onreadystatechange = function() {
			    if (this.readyState == 4 && this.status == 200) {
			    	self.SetCodes(codes, this, self.GetCenter());
			    	/*var kml = new KMLLayer;
			    	kml.mapa = mapa;
			    	kml.setMapa(this);*/
			        //readXML(this);
			    }
			};
		    xhttp.onerror= function(e) {
		        alert("Error fetching " + url);
		    };
		    xhttp.send();			
		}
	};

	Map.prototype.GetTooltips = function(url){
		// Prints the tooltip in search map
		var utils = new Utils();
		var xhttp = utils.OpenConncetion();
		if (xhttp) {
		    xhttp.open("GET", url, true);
		    var self = this;
			xhttp.onreadystatechange = function() {
			    if (this.readyState == 4 && this.status == 200) {
			    	var result = JSON.parse(this.response);
			    	self.contentString = [];
			    	self.name = [];
			    	index = 0;
			    	for (var i = result.length - 1; i >= 0; i--) {
			    		//console.log(self.markers.length);
			    		
			    		obj = result[i];
			    		
				        var request = {
				            location: self.GetCenter(),
				            radius: '5000',
				            query:  obj.logradouro + ',' + obj.numero + '-' + obj.bairro + '-' + obj.cidade + '-' + obj.estado
				        };
				        self.contentString.push(self.SetTooltipDiv(obj.nome, obj.descricao));
				        //self.name = obj.nome;
				        self.name.push(obj.nome);
				        

				        var service = new google.maps.places.PlacesService(self.map);
				        service.textSearch(request, function(results, status){
				        	if (status == google.maps.places.PlacesServiceStatus.OK) {
				              	var marker = self.SetMarker(results[0].geometry.location, false, self.name[index], null, null, null, self.contentString[index]);
				              	marker.addListener('mouseover', function() {
				              		console.log(marker.title);
				              		var div = document.getElementById(marker.title);//self.name);
				              		if(div){
				              			div.style.backgroundColor = "#3e4095";
				              			div.style.color = "#CCC";
				              			div.focus();
				              		}								    
								});
								marker.addListener('mouseout', function() {
								    var div = document.getElementById(marker.title);//self.name);
				              		if(div){
				              			div.style.backgroundColor = "inherit";
				              			div.style.color = "inherit";
				              		}
								});
								index++;
				          	}
				        });
			    	}
			    }
			};
		    xhttp.onerror= function(e) {
		        alert("Error fetching " + url);
		    };
		    xhttp.send();	
		    
		}
	};

	Map.prototype.GetIbgeByCod = function(codes){
		var utils = new Utils();
		var xhttp = utils.OpenConncetion();
		if (xhttp && codes.length > 0) {
			var results = [];
			//this.sencitario_data = [];
			var times = codes.length / 60;
			while(codes.length > 60){
				codes.pop();
			}
			var url = "http://localhost:3000/cod_sencitario/" + this.anuncio_id + "/" + codes.toString() + ".json";
			var self = this;
			xhttp.open("GET", url, true);
			xhttp.onreadystatechange = function() {
			    if (this.readyState == 4 && this.status == 200) {
			    	results.push(JSON.parse(this.response));
			    	alert("Ainda coletando dados da região");
			    	console.log("Mais " + times + " vezes para completar a coleta de dados da região");
			    }
			};
			xhttp.send();
			//for (var i = 0; i < codes.length; i++) {
				/*while(codes.length > 0){
					atual_codes = [];
					for (var i = 0; i < (codes.length/2); i++) {
						atual_codes.push(codes[i]);
					}
					var url = "http://localhost:3000/cod_sencitario/" + atual_codes.toString() + ".json";
					var self = this;
					xhttp.open("GET", url, true);
					xhttp.onreadystatechange = function() {
						//console.log(this.status);
					    if (this.readyState == 4 && this.status == 200) {
					    	results.push(JSON.parse(this.response));
							var qtd_total_mulheres = results[0].qtd_total_mulheres;
						    var qtd_total_homens = results[0].qtd_total_homens;
						    var qtd_familias = results[0].qtd_familias;
						    var qtd_domicilios = results[0].qtd_domicilios;

						    document.getElementById("qtd_total_homens").innerHTML = parseInt(document.getElementById("qtd_total_homens").innerHTML) + qtd_total_homens;
						    document.getElementById("qtd_total_mulheres").innerHTML = parseInt(document.getElementById("qtd_total_mulheres").innerHTML) + qtd_total_mulheres;
						    document.getElementById("total_pessoas").innerHTML = parseInt(document.getElementById("total_pessoas").innerHTML) + qtd_total_mulheres + qtd_total_homens;
						    document.getElementById("total_domicilios").innerHTML = parseInt(document.getElementById("total_domicilios").innerHTML) + qtd_domicilios;
						    document.getElementById("qtd_total_familias").innerHTML = parseInt(document.getElementById("qtd_total_familias").innerHTML) + qtd_familias;
						    document.getElementById("percento_homem").innerHTML = ((qtd_total_homens / (qtd_total_mulheres + qtd_total_homens))*100).toString() + "%";
						    document.getElementById("percento_mulher").innerHTML = ((qtd_total_mulheres / (qtd_total_mulheres + qtd_total_homens))*100).toString() + "%";
					    }
					};
					xhttp.send();



					while(codes.length > 60){
						codes.shift();
					}
				}*/
				/*while(codes.length > 60){
					codes.pop();
				}*/
				/*var url = "http://localhost:3000/cod_sencitario/" + codes.toString() + ".json";
				var self = this;
				xhttp.open("GET", url, true);
				xhttp.onreadystatechange = function() {
					console.log(this.status);
				    if (this.readyState == 4 && this.status == 200) {
				    	//console.log(JSON.parse(this.response));
				    	//self.sencitario_data.push(JSON.parse(this.response));
				    	results.push(JSON.parse(this.response));
				    	//data = JSON.parse(this.response);

				    	console.log(results[0])
						var qtd_total_mulheres = results[0].qtd_total_mulheres;
					    var qtd_total_homens = results[0].qtd_total_homens;
					    var qtd_familias = results[0].qtd_familias;
					    var qtd_domicilios = results[0].qtd_domicilios;

					    document.getElementById("qtd_total_homens").innerHTML = qtd_total_homens;
					    document.getElementById("qtd_total_mulheres").innerHTML = qtd_total_mulheres;
					    document.getElementById("total_pessoas").innerHTML = qtd_total_mulheres + qtd_total_homens;
					    document.getElementById("total_domicilios").innerHTML = qtd_domicilios;
					    document.getElementById("qtd_total_familias").innerHTML = qtd_familias;
					    document.getElementById("percento_homem").innerHTML = ((qtd_total_homens / (qtd_total_mulheres + qtd_total_homens))*100).toString() + "%";
					    document.getElementById("percento_mulher").innerHTML = ((qtd_total_mulheres / (qtd_total_mulheres + qtd_total_homens))*100).toString() + "%";
				    }
				};

				//xhttp.send(JSON.stringify({"cod_sencitario": "" + codes.toString()}));
				xhttp.send();
			    
				
			    xhttp.onerror= function(e) {
			        alert("Error fetching " + url);
			    };*/
			    
			//}
			//alert(codes.length);

		    
			//return result;
		}else{
			console.log("Concluído lazy migration dados da região");
		}
	};
	
/* END GETs methods*/

/********************************************************
*
*					MAPA
*
*********************************************************/