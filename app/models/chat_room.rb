class ChatRoom < ApplicationRecord
  belongs_to :usuario
  has_many :messages, dependent: :destroy
end
