class AnunciosController < ApplicationController
  before_action :set_anuncio, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!, only: [:new, :create, :edit, :update, :destroy]


  def cod_sencitario
    @anuncio = Anuncio.find(params[:id])
    if !@anuncio.nil?
      if @anuncio.codigos.nil?
        cods = params[:cod_sencitario].to_s
      else
        cods = @anuncio.codigos.to_s + "," + params[:cod_sencitario].to_s        
      end
      @anuncio.update(codigos: cods)
      respond_to do |format|
        format.json { render json: true, status: 200 }
      end
    end
    # cod_sencitario = params[:cod_sencitario].split(',') if !params[:cod_sencitario].nil?
    # @qtd_total_homens = 0
    # @qtd_total_mulheres = 0
    # @qtd_familias = 0
    # @qtd_domicilios = 0
    # for code in cod_sencitario
    #   r = Ibge.find_by(cod_sencitario: code)
    #   if r
    #     @qtd_total_homens = @qtd_total_homens + r.qtd_total_homens
    #     @qtd_total_mulheres = @qtd_total_mulheres + r.qtd_total_mulheres
    #     @qtd_familias = @qtd_familias + r.qtd_familias
    #     @qtd_domicilios = @qtd_domicilios + r.qtd_domicilios
    #   end
    # end
    # @ibge = {qtd_total_homens: @qtd_total_homens, qtd_total_mulheres: @qtd_total_mulheres, qtd_familias: @qtd_familias,
    #   qtd_domicilios: @qtd_domicilios}
    # #@ibge = Ibge.find_by(cod_sencitario: cod_sencitario)
    # respond_to do |format|
    #   #format.html # show.html.erb
    #   format.json { render json: @ibge }
    # end
    
  end


  # Pesquisa
  def busca
    @anuncios = Anuncio.search(params)
    if current_usuario.nil?
      @anuncios_like = Array.new
    else
      @anuncios_like = Like.where("usuario = #{current_usuario.id}").order("created_at DESC")
    end
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @result }
    end
  end

  # GET /anuncios
  # GET /anuncios.json
  def index  
    @anuncios = Anuncio.all
    if current_usuario.nil?
      @anuncios_like = []
    else
      @anuncios_like = Like.where("usuario = #{current_usuario.id}").order("created_at DESC")
    end
    
  end


  def test
    respond_to do |format|
      @anuncios = Anuncio.all
      format.json { render json: @anuncios, status: :unprocessable_entity }
    end
  end


  # def cadastro
  #   @anuncio = Anuncio.new
  # end

  # GET /anuncios/1
  # GET /anuncios/1.json
  def show
    @qtd_total_homens = 1
    @qtd_total_mulheres = 1
    @qtd_familias = 1
    @qtd_domicilios = 1

    @qtd_homens_00_10 = 1
    @qtd_homens_11_20 = 1
    @qtd_homens_21_30 = 1
    @qtd_homens_31_40 = 1
    @qtd_homens_41_50 = 1
    @qtd_homens_51_60 = 1
    @qtd_homens_61 = 1
    @qtd_mulheres_00_10 = 1
    @qtd_mulheres_11_20 = 1
    @qtd_mulheres_21_30 = 1
    @qtd_mulheres_31_40 = 1
    @qtd_mulheres_41_50 = 1
    @qtd_mulheres_51_60 = 1
    @qtd_mulheres_61 = 1

    @renda_domicilios = 1

    @renda_homens_A = 1
    @renda_homens_B = 1
    @renda_homens_C = 1
    @renda_homens_D = 1
    @renda_homens_E = 1
    @renda_mulheres_A = 1
    @renda_mulheres_B = 1
    @renda_mulheres_C = 1
    @renda_mulheres_D = 1
    @renda_mulheres_E = 1
    if !@anuncio.codigos_censitarios.nil?
      for code in @anuncio.codigos_censitarios.split(',')
        r = Ibge.find_by(cod_sencitario: code)
        if r
          @qtd_total_homens = @qtd_total_homens + r.qtd_total_homens
          @qtd_total_mulheres = @qtd_total_mulheres + r.qtd_total_mulheres
          @qtd_familias = @qtd_familias + r.qtd_familias
          @qtd_domicilios = @qtd_domicilios + r.qtd_domicilios
          @qtd_homens_00_10 = @qtd_homens_00_10 + r.qtd_homens_00_10
          @qtd_homens_11_20 = @qtd_homens_11_20 + r.qtd_homens_11_20
          @qtd_homens_21_30 = @qtd_homens_21_30 + r.qtd_homens_21_30
          @qtd_homens_31_40 = @qtd_homens_31_40 + r.qtd_homens_31_40
          @qtd_homens_41_50 = @qtd_homens_41_50 + r.qtd_homens_41_50
          @qtd_homens_51_60 = @qtd_homens_51_60 + r.qtd_homens_51_60
          @qtd_homens_61 = @qtd_homens_61 + r.qtd_homens_61
          @qtd_mulheres_00_10 = @qtd_mulheres_00_10 + r.qtd_mulheres_00_10
          @qtd_mulheres_11_20 = @qtd_mulheres_11_20 + r.qtd_mulheres_11_20
          @qtd_mulheres_21_30 = @qtd_mulheres_21_30 + r.qtd_mulheres_21_30
          @qtd_mulheres_31_40 = @qtd_mulheres_31_40 + r.qtd_mulheres_31_40
          @qtd_mulheres_41_50 = @qtd_mulheres_41_50 + r.qtd_mulheres_41_50
          @qtd_mulheres_51_60 = @qtd_mulheres_51_60 + r.qtd_mulheres_51_60
          @qtd_mulheres_61 = @qtd_mulheres_61 + r.qtd_mulheres_61
          @renda_domicilios = @renda_domicilios + r.renda_domicilios if !r.renda_domicilios.nil?
          @renda_homens_A = @renda_homens_A + r.renda_homens_A
          @renda_homens_B = @renda_homens_B + r.renda_homens_B
          @renda_homens_C = @renda_homens_C + r.renda_homens_C
          @renda_homens_D = @renda_homens_D + r.renda_homens_D
          @renda_homens_E = @renda_homens_E + r.renda_homens_E
          @renda_mulheres_A = @renda_mulheres_A + r.renda_mulheres_A
          @renda_mulheres_B = @renda_mulheres_B + r.renda_mulheres_B
          @renda_mulheres_C = @renda_mulheres_C + r.renda_mulheres_C
          @renda_mulheres_D = @renda_mulheres_D + r.renda_mulheres_D
          @renda_mulheres_E = @renda_mulheres_E + r.renda_mulheres_E
        end
      end
    end
    
    if current_usuario.nil?
      @like = nil
    else
      @like = Like.find_by(usuario: current_usuario.id, anuncios_id: @anuncio.id)
    end
    
    @valores = Preco.where(anuncio_id: @anuncio.id)
  end

  # GET /anuncios/new
  def new
    @anuncio = Anuncio.new
  end

  # GET /anuncios/1/edit
  def edit
  end

  # POST /anuncios
  # POST /anuncios.json
  def create
    
    @anuncio = Anuncio.new(anuncio_params)
    @anuncio.usuario_id = current_usuario.id if current_usuario

    respond_to do |format|
      if @anuncio.save
        format.html { redirect_to new_preco_path(anuncio: @anuncio.id), notice: 'Anúncio criado, vamos definir os preços', anuncio: @anuncio }
        format.json { render :show, status: :created, location: @anuncio }
      else
        format.html { render :new }
        format.json { render json: @anuncio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /anuncios/1
  # PATCH/PUT /anuncios/1.json
  def update
    respond_to do |format|
      if @anuncio.update(anuncio_params)
        format.html { redirect_to @anuncio, notice: 'Anuncio was successfully updated.' }
        format.json { render :show, status: :ok, location: @anuncio }
      else
        format.html { render :edit }
        format.json { render json: @anuncio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /anuncios/1
  # DELETE /anuncios/1.json
  def destroy
    @likes = Like.where(anuncios_id: @anuncio.id)
    if ! @likes.empty?
      @likes.each do |like|
        like.destroy
      end      
    end
    @valores = Preco.where(anuncio_id: @anuncio.id)
    if ! @valores.empty?
      @valores.each do |valor|
        valor.destroy
      end      
    end
    @anuncio.destroy
    respond_to do |format|
      format.html { redirect_to :back , notice: 'Anuncio was successfully destroyed.' }
      format.json { head :no_content }
      #anuncios_url
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_anuncio
      @anuncio = Anuncio.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def anuncio_params
      params.require(:anuncio).permit(:pretencao, :tipo_imovel, :local, :qtd_ambientes, :qtd_banheiros, :area_util, :area_total, :nome, :descricao, :logradouro, :numero, :bairro, :cidade, :estado, :cep, :piso, :estacionamento_rua, :estacionamento_privativo, :estacionamento_preco, :estacionamento_tempo, :estacionamento_qtd_vagas, :estacionamento_fluxo, :estacionamento_descricao, :carga_descarga, :carga_descarga_descricao, :preco_aluguel, :preco_condominio, :preco_iptu, :preco_ponto, :preco_negociavel, {fotos: []}, :video, {carga_descarga_fotos: []})
    end

    def search_params
      params.require(:search)
      #params.permit()
    end
end
