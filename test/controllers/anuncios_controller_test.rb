require 'test_helper'

class AnunciosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @anuncio = anuncios(:one)
  end

  test "should get index" do
    get anuncios_url
    assert_response :success
  end

  test "should get new" do
    get new_anuncio_url
    assert_response :success
  end

  test "should create anuncio" do
    assert_difference('Anuncio.count') do
      post anuncios_url, params: { anuncio: { area_total: @anuncio.area_total, area_util: @anuncio.area_util, bairro: @anuncio.bairro, carga_descarga: @anuncio.carga_descarga, carga_descarga_descricao: @anuncio.carga_descarga_descricao, cep: @anuncio.cep, cidade: @anuncio.cidade, descricao: @anuncio.descricao, estacionamento_descricao: @anuncio.estacionamento_descricao, estacionamento_fluxo: @anuncio.estacionamento_fluxo, estacionamento_preco: @anuncio.estacionamento_preco, estacionamento_privativo: @anuncio.estacionamento_privativo, estacionamento_qtd_vagas: @anuncio.estacionamento_qtd_vagas, estacionamento_rua: @anuncio.estacionamento_rua, estacionamento_tempo: @anuncio.estacionamento_tempo, estado: @anuncio.estado, local: @anuncio.local, logradouro: @anuncio.logradouro, nome: @anuncio.nome, numero: @anuncio.numero, piso: @anuncio.piso, preco_aluguel: @anuncio.preco_aluguel, preco_condominio: @anuncio.preco_condominio, preco_iptu: @anuncio.preco_iptu, preco_negociavel: @anuncio.preco_negociavel, preco_ponto: @anuncio.preco_ponto, pretencao: @anuncio.pretencao, qtd_ambientes: @anuncio.qtd_ambientes, qtd_banheiros: @anuncio.qtd_banheiros, tipo_imovel: @anuncio.tipo_imovel } }
    end

    assert_redirected_to anuncio_url(Anuncio.last)
  end

  test "should show anuncio" do
    get anuncio_url(@anuncio)
    assert_response :success
  end

  test "should get edit" do
    get edit_anuncio_url(@anuncio)
    assert_response :success
  end

  test "should update anuncio" do
    patch anuncio_url(@anuncio), params: { anuncio: { area_total: @anuncio.area_total, area_util: @anuncio.area_util, bairro: @anuncio.bairro, carga_descarga: @anuncio.carga_descarga, carga_descarga_descricao: @anuncio.carga_descarga_descricao, cep: @anuncio.cep, cidade: @anuncio.cidade, descricao: @anuncio.descricao, estacionamento_descricao: @anuncio.estacionamento_descricao, estacionamento_fluxo: @anuncio.estacionamento_fluxo, estacionamento_preco: @anuncio.estacionamento_preco, estacionamento_privativo: @anuncio.estacionamento_privativo, estacionamento_qtd_vagas: @anuncio.estacionamento_qtd_vagas, estacionamento_rua: @anuncio.estacionamento_rua, estacionamento_tempo: @anuncio.estacionamento_tempo, estado: @anuncio.estado, local: @anuncio.local, logradouro: @anuncio.logradouro, nome: @anuncio.nome, numero: @anuncio.numero, piso: @anuncio.piso, preco_aluguel: @anuncio.preco_aluguel, preco_condominio: @anuncio.preco_condominio, preco_iptu: @anuncio.preco_iptu, preco_negociavel: @anuncio.preco_negociavel, preco_ponto: @anuncio.preco_ponto, pretencao: @anuncio.pretencao, qtd_ambientes: @anuncio.qtd_ambientes, qtd_banheiros: @anuncio.qtd_banheiros, tipo_imovel: @anuncio.tipo_imovel } }
    assert_redirected_to anuncio_url(@anuncio)
  end

  test "should destroy anuncio" do
    assert_difference('Anuncio.count', -1) do
      delete anuncio_url(@anuncio)
    end

    assert_redirected_to anuncios_url
  end
end
