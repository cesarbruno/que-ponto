class UpdateAnuncios < ActiveRecord::Migration[5.0]
  def change
  	remove_column :anuncios, :local
	remove_column :anuncios, :estacionamento_preco
	remove_column :anuncios, :estacionamento_tempo
	remove_column :anuncios, :estacionamento_fluxo
	remove_column :anuncios, :preco_aluguel
	remove_column :anuncios, :preco_condominio
	remove_column :anuncios, :preco_iptu
	remove_column :anuncios, :preco_ponto
	remove_column :anuncios, :preco_negociavel
	remove_column :anuncios, :created_at
	remove_column :anuncios, :updated_at
	remove_column :anuncios, :usuario_id
	remove_column :anuncios, :estruturado_negocio
	remove_column :anuncios, :estruturado_negocio_descricao
	remove_column :anuncios, :imovel_viavel
	remove_column :anuncios, :corretores
	remove_column :anuncios, :comissao_aluguel
	remove_column :anuncios, :comissao_venda
	remove_column :anuncios, :contato_apenas_corretor
	remove_column :anuncios, :mezanino
	remove_column :anuncios, :armazem
	remove_column :anuncios, :esquina
	remove_column :anuncios, :area_externa
	remove_column :anuncios, :cadeirante
	remove_column :anuncios, :cadeirante_descricao
	remove_column :anuncios, :estacionamento_compartilhado
	remove_column :anuncios, :codigos

	add_column :anuncios, :codigo, :string
	add_column :anuncios, :codigos_censitarios, :string
	add_column :anuncios, :estacionamento_coletivo, :boolean
	add_column :anuncios, :estacionamento_pago, :boolean
	add_column :anuncios, :adaptado, :boolean
	add_column :anuncios, :adaptado_descricao, :string
	add_column :anuncios, :fluxo_15min, :string
	add_column :anuncios, :fluxo_medio_diario, :string
	add_column :anuncios, :fluxo_horario_pico, :string
	add_column :anuncios, :fluxo_pior_dia, :string
	add_column :anuncios, :tipo_negocio, :string
	add_column :anuncios, :segmento, :string
	add_column :anuncios, :idade_empresa, :integer
	add_column :anuncios, :motivo_venda, :string
	add_column :anuncios, :qtd_funcionarios, :integer
	add_column :anuncios, :faturamento_bruto, :float
	add_column :anuncios, :lucro_medio, :float
	add_column :anuncios, :capital_giro, :float
	add_column :anuncios, :valor_estoque, :float
  end
end
